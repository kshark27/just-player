package shoozhoo.libandrotranslation;

import java.io.Closeable;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class LibAndTransUtil {
	public static void log(String s){
		Log.i("LibAndroTrans", s);
	}
	public static void log(String s, Throwable t){
		Log.i("LibAndroTrans", s, t);
	}
	public static void safeclose(Closeable c){
		if(c!=null){
			try{
				c.close();
			}catch (Exception e) {}
		}
	}

	private static String KEY_APPTRANSLATION = "KEY_APPTRANSLATION";
	private static String KEY_LANG = "KEY_LANG";
	public static String getTranslationLang(Context ctx){
		SharedPreferences pref = ctx.getSharedPreferences(KEY_APPTRANSLATION, Context.MODE_PRIVATE);
		if(pref==null){
			return "";
		}
		return pref.getString(KEY_LANG, Locale.getDefault().getLanguage());
	}
	public static void saveTranslationLang(Context ctx, String lang){
		SharedPreferences pref = ctx.getSharedPreferences(KEY_APPTRANSLATION, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(KEY_LANG, lang);
		editor.commit();
	}
}
