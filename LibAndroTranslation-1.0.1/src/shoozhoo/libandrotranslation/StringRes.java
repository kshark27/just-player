package shoozhoo.libandrotranslation;

import android.os.Parcel;
import android.os.Parcelable;

public class StringRes implements Parcelable{
	private String resourceName;
	private int resId;
	private String defString;
	private String userString;

	public StringRes(String resName, int resId, String defStr, String userString){
		this.resourceName = resName;
		this.resId = resId;
		this.defString = defStr;
		this.userString = userString;
	}

	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public int getResId() {
		return resId;
	}
	public void setResId(int resId) {
		this.resId = resId;
	}
	public String getDefString() {
		return defString;
	}
	public void setDefString(String defString) {
		this.defString = defString;
	}
	public String getUserString() {
		return userString;
	}
	public void setUserString(String userString) {
		this.userString = userString;
	}

	public static final Parcelable.Creator<StringRes> CREATOR
	= new Parcelable.Creator<StringRes>() {
		public StringRes createFromParcel(Parcel in) {
			return new StringRes(in);
		}

		public StringRes[] newArray(int size) {
			return new StringRes[size];
		}
	};

	private StringRes(Parcel in){
		resourceName = in.readString();
		resId = in.readInt();
		defString = in.readString();
		userString = in.readString();
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(resourceName);
		dest.writeInt(resId);
		dest.writeString(defString);
		dest.writeString(userString);
	}


}
