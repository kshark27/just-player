package jp.co.kayo.android.localplayer.menu;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TreeItem;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.ActionMode;
import android.view.ActionMode.Callback;

import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

public class MultipleTreeChoiceMediaContentActionCallback extends
        MultipleChoiceMediaContentActionCallback implements
        OnChildClickListener, OnGroupClickListener {
    ExpandableListView mExpandableListView;

    public MultipleTreeChoiceMediaContentActionCallback(Context context,
            ExpandableListView listView, Handler handler) {
        super(context, listView, handler);
        mExpandableListView = listView;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v,
            int groupPosition, long id) {
        if(mMode!=null){
            boolean check = !parent.isItemChecked(flatPosition(groupPosition));
            parent.setItemChecked(
                    flatPosition(groupPosition), check);
            selectItem(-1);
            return true;
        }
        return false;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
        if(mMode!=null){
            boolean check = !parent.isItemChecked(flatPosition(groupPosition, childPosition));
            parent.setItemChecked(
                    flatPosition(groupPosition, childPosition),check) ;
            selectItem(-1);
            return true;
        }
        return false;
    }
    
    @Override
    public Object makeSelectedItems(){
        List<TreeItem> selectedItems = new ArrayList<TreeItem>();
        SparseBooleanArray checked = mExpandableListView.getCheckedItemPositions();
        for(int i=0; i<checked.size(); i++){
            if(checked.valueAt(i)){
                int pos = checked.keyAt(i);
                long flatPos = mExpandableListView.getExpandableListPosition(pos);
                if(ExpandableListView.getPackedPositionType (flatPos) == ExpandableListView.PACKED_POSITION_TYPE_GROUP){
                    int groupPos = ExpandableListView.getPackedPositionGroup(flatPos);
                    selectedItems.add(new TreeItem(groupPos));
                }
                else{
                    int groupPos = ExpandableListView.getPackedPositionGroup(flatPos);
                    int childPos = ExpandableListView.getPackedPositionChild(flatPos);
                    selectedItems.add(new TreeItem(groupPos, childPos));
                }
            }
        }
        
        return selectedItems;
    }
    
    private int flatPosition(int groupPosition) {
        return mExpandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(groupPosition));

    }
    
    private int flatPosition(int groupPosition, int childPosition) {
        return mExpandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));

    }
}
