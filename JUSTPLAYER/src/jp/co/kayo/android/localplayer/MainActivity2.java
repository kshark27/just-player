package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.fragment.AlbumGridViewFragment;
import jp.co.kayo.android.localplayer.fragment.AlbumListViewFragment;
import jp.co.kayo.android.localplayer.fragment.AlbumSongsFragment;
import jp.co.kayo.android.localplayer.fragment.ArtistExpandViewFragment;
import jp.co.kayo.android.localplayer.fragment.ArtistListFragment;
import jp.co.kayo.android.localplayer.fragment.ArtistListViewFragment;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.fragment.EqualizerFragment;
import jp.co.kayo.android.localplayer.fragment.GenresListFragment;
import jp.co.kayo.android.localplayer.fragment.MainFragment;
import jp.co.kayo.android.localplayer.fragment.MediaListViewFragment;
import jp.co.kayo.android.localplayer.fragment.PlaybackListViewFragment;
import jp.co.kayo.android.localplayer.fragment.PlaylistFragment;
import jp.co.kayo.android.localplayer.fragment.PlaylistViewFragment;
import jp.co.kayo.android.localplayer.menu.DataSourceMenu;
import jp.co.kayo.android.localplayer.menu.ShareMenu;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.LocalSuggestionProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.NfcHelper.BeamMessage;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils.TruncateAt;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

@SuppressLint("NewApi")
@TargetApi(8)
public class MainActivity2 extends BaseActivity implements OnQueryTextListener,
        TabListener {
    MyPreferenceManager mPref;
    ViewPager mViewPager;
    private BillingService mBillingService = null;
    DungeonsPurchaseObserver mDungeonsPurchaseObserver;
    private ViewCache mViewCache;
    public MyTabsAdapter mTabsAdapter;
    Handler mHandler = new MyHandler(this);
    private Intent oneIntent;
    private SearchView mSearchView;
    private ColorSet mColorset;

    private static class MyHandler extends Handler {
        WeakReference<MainActivity2> ref;

        MyHandler(MainActivity2 r) {
            ref = new WeakReference<MainActivity2>(r);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity2 main = ref.get();
            if (main != null) {
                if (msg.what == SystemConsts.EVT_DSCHANFED) {
                    try {
                        IMediaPlayerService binder = main.getBinder();
                        if (binder != null) {
                            binder.setContentsKey(null);
                            binder.reset();
                            // binder.clear();
                            binder.reload();
                        }
                    } catch (RemoteException e) {
                    }

                    if (main.getSupportFragmentManager()
                            .getBackStackEntryCount() > 0) {
                        main.getSupportFragmentManager().popBackStack();
                        main.mPref.setResumeReloadFlag(true);
                    } else {
                        if (main.mTabsAdapter != null) {
                            for (int i = 0; i < main.mTabsAdapter.getCount(); i++) {
                                ContentManager mgr = (ContentManager) main.mTabsAdapter
                                        .getFragment(i);
                                if (mgr != null) {
                                    mgr.reload();
                                }
                            }
                        } else {
                            ContentManager mgr = (ContentManager) main
                                    .getSupportFragmentManager()
                                    .findFragmentById(R.id.container);
                            if (mgr != null) {
                                mgr.reload();
                            }
                        }
                    }
                } else if (msg.what == SystemConsts.EVT_SELECT_PLAYBACK_CLEAR) {
                    IMediaPlayerService binder = main.getBinder();
                    if (binder != null) {
                        try {
                            binder.setContentsKey(null);
                            binder.clear();
                        } catch (RemoteException e) {
                        }
                    }
                } else if (msg.what == SystemConsts.EVT_UPDATE_LISTVIEW) {
                    if (main.mTabsAdapter != null) {
                        main.mTabsAdapter.notifyDataSetChanged();
                    }
                } else if (msg.what == SystemConsts.EVT_FORCE_RELOAD) {
                    int type = (Integer) msg.obj;
                    if (main.mTabsAdapter != null) {
                        int position = main.selectedTabPosition();
                        if (position != -1) {
                            MainFragment f = (MainFragment) main.mTabsAdapter
                                    .getFragment(position);
                            ContentManager mgr = (ContentManager) f
                                    .currentFragment();
                            foreReload(mgr, type);
                        }
                    } else {
                        Fragment fragment = main.getSupportFragmentManager().findFragmentById(
                                        R.id.container);
                        if(fragment instanceof ContentManager){
                            ContentManager cm = (ContentManager)fragment;
                            foreReload(cm, type);
                        }
                    }
                }
            }
        }

        private void foreReload(ContentManager mgr, int type) {
            if (mgr != null) {
                if (type == 0) {
                    // Album
                    if (mgr instanceof AlbumGridViewFragment
                            || mgr instanceof AlbumListViewFragment
                            || mgr instanceof ArtistExpandViewFragment
                            || mgr instanceof PlaylistViewFragment) {
                        mgr.reload();
                    }
                } else if (type == 1) {
                    // Artist
                    if (mgr instanceof ArtistListViewFragment
                            || mgr instanceof ArtistExpandViewFragment) {
                        mgr.reload();
                    }
                } else if (type == 2) {
                    // Media
                    if (mgr instanceof MediaListViewFragment
                            || mgr instanceof AlbumSongsFragment
                            || mgr instanceof ArtistListFragment
                            || mgr instanceof GenresListFragment
                            || mgr instanceof PlaylistFragment) {
                        mgr.changedMedia();
                    }
                }
            }
        }
    }

    IMediaPlayerServiceCallback.Stub mCallback = new IMediaPlayerServiceCallback.Stub() {
        ControlFragment control;

        ControlFragment getControll() {
            if (control == null) {
                FragmentManager m = getSupportFragmentManager();
                control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
            }
            return control;
        }

        @Override
        public void updateView(final boolean updatelist) throws RemoteException {
            Logger.d("updateView=" + updatelist);
            // もし、コントロール部分が表示されているならリストを更新してあげて
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // AlbumList,ArtistList,Song等
                        ControlFragment control = getControll();
                        // 再生中の曲の更新
                        if (mBinder != null) {
                            try {
                                mViewCache.setPosition(mBinder.getPosition(),
                                        mBinder.getMediaId(),
                                        mBinder.getPrefetchId());
                            } catch (RemoteException e) {
                            }
                        }

                        // 更新処理
                        if (control != null) {
                            control.updateView();
                        }

                        if (mTabsAdapter != null) {
                            for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                                Fragment f = mTabsAdapter.getFragment(i);
                                if(f!=null){
                                    Fragment f2 = getSupportFragmentManager()
                                            .findFragmentById(
                                                    FragmentUtils.getFragmentId(
                                                            MainActivity2.this, f));
                                    reloadFragment(f2, updatelist);
                                }
                            }
                        } else {
                            Fragment f = getSupportFragmentManager()
                                    .findFragmentById(R.id.container);
                            reloadFragment(f, updatelist);
                        }
                        Fragment f = getSupportFragmentManager()
                                .findFragmentByTag(SystemConsts.TAG_EQUALIZER);
                        if (f != null && f instanceof EqualizerFragment) {
                            EqualizerFragment eqf = (EqualizerFragment) f;
                            eqf.changedMedia();
                        }
                    }
                });
            }
        }

        private void reloadFragment(Fragment f, boolean updatelist) {
            if (f != null) {
                if (updatelist && f instanceof PlaybackListViewFragment) {
                    ((PlaybackListViewFragment) f).reload();
                } else {
                    ((ContentManager) f).changedMedia();
                }
                if (updatelist && f instanceof ContextMenuFragment) {
                    ((ContextMenuFragment) f).hideMenu();
                }
            }
        }

        @Override
        public void updateList() throws RemoteException {
            // もし、再生中のフラグメントが表示されているならリストを更新してあげて
        }

        @Override
        public void onBufferingUpdate(int percent) throws RemoteException {
            // バッファリング中のプログレスバーですよぉ
            ControlFragment f = getControll();
            if (f != null) {
                f.onBufferingUpdate(percent);
            }
        }

        @Override
        public void startProgress(long max) throws RemoteException {
            long media_id = mBinder.getMediaId();
            int pos = mBinder.getPosition();
            long pref_id = mBinder.getPrefetchId();
            mViewCache.setPosition(pos, media_id, pref_id);
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).startProgress(max);
            }
        }

        @Override
        public void stopProgress() throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).stopProgress();
            }
        }

        @Override
        public void progress(long pos, long max) throws RemoteException {
            Fragment current = getCurrentFragment();
            if (current != null && current instanceof IProgressView) {
                ((IProgressView) current).progress(pos, max);
            }
        }

        @Override
        public void close() throws RemoteException {
            MainActivity2.this.finish();
        }
    };

    ContentObserver mAlbumContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            mHandler.sendMessage(mHandler.obtainMessage(
                    SystemConsts.EVT_FORCE_RELOAD, 0));
        }
    };

    ContentObserver mArtistContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            mHandler.sendMessage(mHandler.obtainMessage(
                    SystemConsts.EVT_FORCE_RELOAD, 1));
        }
    };

    ContentObserver mMediaContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            mHandler.sendMessage(mHandler.obtainMessage(
                    SystemConsts.EVT_FORCE_RELOAD, 2));
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SystemConsts.REQUEST_TAGEDIT) {
            // 変更を通知
            if (mTabsAdapter != null) {
                for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                    ContentManager f = (ContentManager) mTabsAdapter
                            .getFragment(i);
                    if (f != null) {
                        f.reload();
                    }
                }
            } else {
                ContentManager f = (ContentManager) getSupportFragmentManager()
                        .findFragmentById(R.id.container);
                if (f != null) {
                    f.reload();
                }
            }
        } else if (requestCode == SystemConsts.REQUEST_DSCHANGED) {
            mHandler.sendEmptyMessage(SystemConsts.EVT_DSCHANFED);
        } else if (requestCode == SystemConsts.REQUEST_ALBUMART) {
            if (data != null) {
                String album_key = data.getStringExtra("album_key");
                if (album_key != null) {
                    ContentsUtils.reloadAlbumArt(this, album_key);
                    // 変更を通知
                    if (mTabsAdapter != null) {
                        for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                            ContentManager f = (ContentManager) mTabsAdapter
                                    .getFragment(i);
                            if (f != null) {
                                f.reload();
                            }
                        }
                    } else {
                        ContentManager f = (ContentManager) getSupportFragmentManager()
                                .findFragmentById(R.id.container);
                        if (f != null) {
                            f.reload();
                        }
                    }
                }
            }
        } else if (requestCode == SystemConsts.REQUEST_CONFIG) {
            if (resultCode == SystemConsts.RESULT_REBOOT) {
                // アプリを再起動する
                rebootApp();
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        mPref = new MyPreferenceManager(this);
        mColorset = new ColorSet();
        mColorset.load(this);

        int tabcount = mPref.getTabCount();
        boolean useSwipe = mPref.isUseSwipe();
        if (useSwipe) {
            setContentView(R.layout.main_tab);
            // setup
            mViewPager = (ViewPager) findViewById(R.id.pager);
            mTabsAdapter = new MyTabsAdapter(this, getSupportActionBar(),
                    mViewPager);
            for (int i = 0; i < tabcount; i++) {
                ActionBar.Tab maintab = getSupportActionBar().newTab().setText(
                        getString(R.string.lb_tab_blank));
                Bundle bundle1 = new Bundle();
                bundle1.putInt("tabposition", i);
                bundle1.putString("tagname", "tab" + (i + 1));
                mTabsAdapter.addTab(maintab, MainFragment.class, bundle1);
            }
        } else {
            setContentView(R.layout.main_tab2);
            for (int i = 0; i < tabcount; i++) {
                Bundle bundle1 = new Bundle();
                bundle1.putInt("tabposition", i);
                bundle1.putString("tagname", "tab" + (i + 1));
                String tabname = FragmentUtils.getDefaultTabName(this, FragmentUtils.getTabKind(this, i));
                Tab tab = getSupportActionBar().newTab();
                View view = createTabView(this, tabname);
                view.setTag(tab);
                tab.setCustomView(view);
                tab.setTabListener(this);
                tab.setTag(bundle1);
                getSupportActionBar().addTab(tab);
            }
        }

        //ActionBar
        hideProgressBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(
                getSupportFragmentManager().getBackStackEntryCount() > 0);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getSupportActionBar().setSubtitle(getString(R.string.hello));

        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        // SDCardChec
        Funcs.checkSDCard(this);

        // Audio Volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);

        getSupportFragmentManager().addOnBackStackChangedListener(
                onBackStackChangedListener);

        // ContentResolver
        getContentResolver().registerContentObserver(
                MediaConsts.ALBUM_CONTENT_URI, true, mAlbumContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.ARTIST_CONTENT_URI, true, mArtistContentObserver);
        getContentResolver().registerContentObserver(
                MediaConsts.MEDIA_CONTENT_URI, true, mMediaContentObserver);

        Intent intent = getIntent();
        if (intent == null
                || intent.getAction() == null
                || !intent.getAction()
                        .equals(SystemConsts.MAIN_ACITON_SHOWHOMW)) {
            // 初回起動か確認
            boolean isFirst = mPref.isFirstBoot();
            if (isFirst) {
                mPref.setFirstBoot(false);
                // まず、既にアプリを購入済みか確認する
                mBillingService = new BillingService();
                mDungeonsPurchaseObserver = new DungeonsPurchaseObserver(this,
                        mBillingService, new Handler());
                ResponseHandler.register(mDungeonsPurchaseObserver);
                mBillingService.setContext(this);
                mBillingService.restoreTransactions();

            }
        }

        if (savedInstanceState != null) {
            int index = savedInstanceState.getInt("index");
            selectTab(index);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if (procIntent(intent) == false) {
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        procIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            doSearchWithIntent(intent);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            if (intent.getFlags() == Intent.FLAG_ACTIVITY_NEW_TASK) {
                doSearchWithIntent(intent);
            }
        }
    }

    @Override
    public void onServiceConnected(IMediaPlayerService binder) {
        if (oneIntent != null) {
            revieveNdef(oneIntent);
        }
        if (mTabsAdapter != null) {
            for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                if (f != null) {
                    if (f instanceof PlaybackListViewFragment) {
                        f.reload();
                    } else {
                        f.changedMedia();
                    }
                }
            }
        } else {
            ContentManager f = (ContentManager) getSupportFragmentManager()
                    .findFragmentById(R.id.container);
            if (f != null) {
                if (f instanceof PlaybackListViewFragment) {
                    f.reload();
                } else {
                    f.changedMedia();
                }
            }
        }
        Fragment f = getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_EQUALIZER);
        if (f != null && f instanceof EqualizerFragment) {
            EqualizerFragment eqf = (EqualizerFragment) f;
            eqf.onServiceConnected(binder);
        }
    }

    private boolean procIntent(Intent intent) {
        if (intent != null) {
            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
                if (revieveNdef(intent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean revieveNdef(Intent intent) {
        if (getNFCHelper() != null) {
            IMediaPlayerService binder = getBinder();
            if (binder != null) {
                oneIntent = null;
                ArrayList<BeamMessage> list = getNFCHelper().getNdefMessage(
                        intent);
                if (list != null) {
                    try {
                        String cntkey = SystemConsts.CONTENTSKEY_PLAYLIST
                                + System.currentTimeMillis();
                        try {
                            binder.setContentsKey(cntkey);
                            binder.lockUpdateToPlay();
                            binder.clearcut();
                            for (int i = 0; i < list.size(); i++) {
                                BeamMessage msg = list.get(i);
                                try {
                                    binder.addMediaD(0, msg.duration,
                                            msg.title, msg.album, msg.artist,
                                            msg.url);
                                } catch (RemoteException e) {
                                }
                            }
                        } finally {
                            binder.play();
                        }
                    } catch (RemoteException e) {
                    }
                    return true;
                }
            }
        }
        oneIntent = intent;
        return false;
    }

    public int selectedTabPosition() {
        Tab tab = getSupportActionBar().getSelectedTab();
        if (tab != null) {
            int position = tab.getPosition();
            return position;
        }
        return -1;
    }

    public void doSearchWithIntent(Intent intent) {
        String queryString = intent.getStringExtra(SearchManager.QUERY);
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY,
                LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);
        if (mTabsAdapter != null) {
            int position = selectedTabPosition();
            if (position != -1) {
                ContextMenuFragment f = (ContextMenuFragment) mTabsAdapter
                        .getFragment(position);
                if (f != null) {
                    f.doSearchQuery(queryString);
                }
            }
        } else {
            ContextMenuFragment f = (ContextMenuFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.container);
            if (f != null) {
                f.doSearchQuery(queryString);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("index", getSupportActionBar()
                .getSelectedNavigationIndex());
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 音楽サービスとの接続
        if (mBinder == null) {
            Intent service = new Intent(this, MediaPlayerService.class);
            bindService(service, mConnection, 0);
            startService(service);
        }
        if (mTabsAdapter != null) {
            for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                ContentManager f = (ContentManager) mTabsAdapter.getFragment(i);
                if (f != null) {
                    Tab tab = getSupportActionBar().getTabAt(i);
                    String tabname = f.getName(this);
                    setTabText(tab, tabname);
                }
            }
        } else {
            // TODO
            /*
             * MainFragment f =
             * (MainFragment)getSupportFragmentManager().findFragmentById
             * (R.id.container); if(f!=null){ String tabname =
             * f.getName(MainActivity2.this);
             * setTabText(getSupportActionBar().getSelectedTab(), tabname); }
             */
        }

        View controlspacer = findViewById(R.id.controlspacer);
        if (controlspacer != null) {
            long time = mPref.getHideTime();
            if (time == 0) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        1, (int) getResources().getDimension(
                                R.dimen.controlspacer_height));
                controlspacer.setLayoutParams(params);
                LinearLayout ll = (LinearLayout) controlspacer.getParent();
                ll.requestLayout();
            } else {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        0, 0);
                controlspacer.setLayoutParams(params);
                LinearLayout ll = (LinearLayout) controlspacer.getParent();
                ll.requestLayout();
            }
        }
    }

    @Override
    protected void onPause() {
        if (mBinder != null) {
            try {
                mBinder.unregisterCallback(mCallback);
                mBinder = null;
            } catch (RemoteException e) {
            }
            unbindService(mConnection);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        getContentResolver().unregisterContentObserver(mAlbumContentObserver);
        getContentResolver().unregisterContentObserver(mArtistContentObserver);
        getContentResolver().unregisterContentObserver(mMediaContentObserver);

        if (mDungeonsPurchaseObserver != null) {
            ResponseHandler.unregister(mDungeonsPurchaseObserver);
        }
        if (mBillingService != null) {
            mBillingService.unbind();
        }

        ViewCache.clearImage();

        super.onDestroy();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                Fragment fragment = getCurrentFragment();
                if (fragment != null) {
                    if (fragment instanceof ContextMenuFragment) {
                        ContextMenuFragment f = (ContextMenuFragment) fragment;
                        if (f.onBackPressed()) {
                            return true;
                        }
                    }
                }

                if (mPref.isExitCheck()) {
                    // ダイアログの表示
                    AlertDialog.Builder ad = new AlertDialog.Builder(this);
                    ad.setMessage(getString(R.string.alert_exit_msg));
                    ad.setPositiveButton(getString(R.string.lb_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                    // OKならActivity終了
                                    finish();
                                }
                            });
                    ad.setNegativeButton(getString(R.string.lb_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                        int whichButton) {
                                }
                            });
                    ad.create();
                    ad.show();
                    return true;
                } else {
                    return super.dispatchKeyEvent(event);
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    MenuItem mSearchItem;

    private void setupSearchView(MenuItem searchItem) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
        }
        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Logger.d("onQueryTextSubmit");

        String queryString = newText;
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                LocalSuggestionProvider.AUTHORITY,
                LocalSuggestionProvider.DATABASE_MODE_QUERIES);
        suggestions.saveRecentQuery(queryString, null);

        if (mTabsAdapter != null) {
            int position = selectedTabPosition();
            if (position != -1) {
                ContextMenuFragment f = (ContextMenuFragment) mTabsAdapter
                        .getFragment(position);
                if (f != null) {
                    f.doSearchQuery(queryString);
                }
            }
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if(fragment instanceof ContextMenuFragment){
                ContextMenuFragment cm = (ContextMenuFragment)fragment;
                if (cm != null) {
                    cm.doSearchQuery(queryString);
                }
            }
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Logger.d("onQueryTextSubmit");
        mSearchItem.collapseActionView();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main_menu_items, menu);

        mSearchItem = menu.findItem(R.id.mnu_search);
        mSearchView = (SearchView) mSearchItem.getActionView();
        if (Build.VERSION.SDK_INT >= 8) {
            setupSearchView(mSearchItem);
        }

        if (Build.VERSION.SDK_INT < 9) {
            MenuItem item = menu.findItem(R.id.mnu_equalizer);
            if (item != null) {
                item.setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu == null) {
            return super.onPrepareOptionsMenu(menu);
        }

        MenuItem mnu_ds = menu.findItem(R.id.mnu_ds);
        DataSourceMenu ds_provider = (DataSourceMenu) mnu_ds
                .getActionProvider();
        if (ds_provider != null) {
            ds_provider.setActivity(this, mHandler);
        }

        MenuItem mnu_share = menu.findItem(R.id.mnu_share);
        ShareMenu share_provider = (ShareMenu) mnu_share.getActionProvider();
        if (share_provider != null) {
            share_provider.setBinder(getBinder());
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
            case android.R.id.home: {
                getSupportFragmentManager().popBackStack();
            }
                break;
            case R.id.mnu_config: {
                // 設定画面
                Intent intent = new Intent(this, ConfigActivity.class);
                startActivityForResult(intent, SystemConsts.REQUEST_CONFIG);
            }
                break;
            case R.id.mnu_search: {
                onSearchRequested();
            }
                break;
            case R.id.mnu_clear_playlist: {
                mHandler.sendEmptyMessage(SystemConsts.EVT_SELECT_PLAYBACK_CLEAR);
            }
                break;
            case R.id.mnu_equalizer: {
                // イコライザー画面
                Intent intent = new Intent(this, EqualizerActivity.class);
                startActivity(intent);
            }
                break;
            default: {
            }
            }
            return super.onOptionsItemSelected(item);
        } finally {
            // Google Analyticsにトラックイベントを送信
        }
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        return mCallback;
    }

    @Override
    ViewCache getViewCache() {
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        return mHandler;
    }

    public void rebootApp() {
        finish();
        startActivity(new Intent(this, MainActivity2.class));
    }

    
    @Override
    public void onTabSelected(Tab tab, android.app.FragmentTransaction arg1) {
        Fragment old = getSupportFragmentManager().findFragmentById(
                R.id.container);
        if (old != null && old instanceof ContextMenuFragment) {
            ContextMenuFragment menufragment = (ContextMenuFragment) old;
            menufragment.hideMenu();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate(
                    SystemConsts.TAG_SUBFRAGMENT,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        Bundle args = (Bundle) tab.getTag();
        int tabposition = args.getInt("tabposition");
        int tabkind = FragmentUtils.getTabKind(this, tabposition);
        Fragment fragment = FragmentUtils.createFragment(tabkind);
        fragment.setArguments(args);
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        AnimationHelper.setFragmentToTabClick(mPref, t);
        if(old!=null){
            t.hide(old);
        }
        t.replace(R.id.container, fragment);
        t.commit();
        setTabText(tab, ((ContentManager) fragment).getName(this));
    }

    @Override
    public void onTabUnselected(Tab tab, android.app.FragmentTransaction arg1) {

    }

    @Override
    public void onTabReselected(Tab tab, android.app.FragmentTransaction arg1) {
        Fragment f = getSupportFragmentManager().findFragmentById(
                R.id.container);
        if (f instanceof ContextMenuFragment && f.isVisible()) {
            String tabname = ((ContextMenuFragment) f).selectSort();
            if (tabname != null) {
                setTabText(tab, tabname);
            }
        }

    }

    public void setTabText(Tab tab, String tabname) {
        if (mTabsAdapter != null) {
            mTabsAdapter.setTabText(tab, tabname);
        } else {
            TextView view = (TextView) tab.getCustomView();
            if (view != null) {
                view.setText(tabname!=null?tabname.toUpperCase():"");
                mHandler.sendEmptyMessage(SystemConsts.EVT_UPDATE_LISTVIEW);
            }else{
                tab.setText(tabname);
            }
        }
    }
    
    private View createTabView(Context context, String text) {
        if (text != null) {
            TextView textView = new TextView(context, null, android.R.attr.actionBarTabTextStyle);
            //TextView textView = new TextView(context, null, R.attr.actionBarTabTextStyle);
            textView.setEllipsize(TruncateAt.END);
            LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.CENTER_VERTICAL;
            textView.setLayoutParams(lp);
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setText(text);
            int color = mColorset.getColor(ColorSet.KEY_ACTIONBAR_TAB_COLOR);
            if (color != -1) {
                textView.setTextColor(color);
            }

            textView.setOnClickListener(tabClickListner);
            textView.setOnLongClickListener(tabLongClickListener);

            return textView;
        }
        else {
            return null;
        }
    }
    
    private OnClickListener tabClickListner = new OnClickListener(){

        @Override
        public void onClick(View v) {
            final Tab tab = getSupportActionBar().getSelectedTab();
            View currelntView = tab.getCustomView();
            if (currelntView == v) {
                Logger.d("ReSelect Click!");
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
                if (f instanceof ContextMenuFragment) {
                    String tabname = ((ContextMenuFragment) f).selectSort();
                    if (tabname != null) {
                        setTabText(tab, tabname);
                    }
                }
            } else {
                Tab viewTab = (Tab) v.getTag();
                if (viewTab != null) {
                    getSupportActionBar().selectTab(viewTab);
                }
            }
        }
        
    };
    
    private OnLongClickListener tabLongClickListener = new OnLongClickListener(){

        @Override
        public boolean onLongClick(View v) {
            final Tab tab = getSupportActionBar().getSelectedTab();
            View currentView = tab.getCustomView();
            if (currentView == v && getSupportFragmentManager().getBackStackEntryCount() == 0) {
                Logger.d("ReSelect Long Click!");
                final int tabposotion = tab.getPosition();
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity2.this);
                final CharSequence[] selectViews = {
                        getString(R.string.lb_tab_albums),
                        getString(R.string.lb_tab_albums_list),
                        getString(R.string.lb_tab_artist),
                        getString(R.string.lb_tab_artist_expand),
                        getString(R.string.lb_tab_media),
                        getString(R.string.lb_tab_playlist),
                        getString(R.string.lb_tab_folder),
                        getString(R.string.lb_tab_genres),
                        getString(R.string.lb_tab_videos),
                        getString(R.string.lb_tab_order)
                };
                final int[] selectKind = {
                        SystemConsts.TAG_ALBUM_GRID,
                        SystemConsts.TAG_ALBUM_LIST,
                        SystemConsts.TAG_ARTIST_LIST,
                        SystemConsts.TAG_ARTIST_EXPAND,
                        SystemConsts.TAG_MEDIA,
                        SystemConsts.TAG_PLAYLIST,
                        SystemConsts.TAG_FOLDER,
                        SystemConsts.TAG_GENRES,
                        SystemConsts.TAG_VIDEO,
                        SystemConsts.TAG_PLAYBACK
                };
                alertDialogBuilder.setTitle(getString(R.string.lb_tab_title));
                alertDialogBuilder.setItems(selectViews, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int kind) {
                        Fragment old = getSupportFragmentManager().findFragmentById(R.id.container);
                        FragmentUtils.saveTabKind(MainActivity2.this, old, kind);
                        Bundle args = FragmentUtils.cloneBundle(old);
                        
                        Fragment fragment = FragmentUtils.createFragment(kind);
                        fragment.setArguments(args);
                        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToTabClick(mPref, t);
                        if(old!=null){
                            t.hide(old);
                        }
                        t.replace(R.id.container, fragment);
                        t.commit();
                        setTabText(tab, ((ContentManager) fragment).getName(MainActivity2.this));
                    }
                });

                // ダイアログを表示
                alertDialogBuilder.create().show();
                return true;
            }
            return false;
        }
        
    };

    private OnBackStackChangedListener onBackStackChangedListener = new OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            getSupportActionBar().setDisplayHomeAsUpEnabled(
                    getSupportFragmentManager().getBackStackEntryCount() > 0);

            try {
                boolean reload = mPref.isResumeReloadFlag();
                if (mTabsAdapter != null) {
                    for (int i = 0; i < mTabsAdapter.getCount(); i++) {
                        Tab tab = getSupportActionBar().getTabAt(i);
                        ContentManager mgr = (ContentManager) mTabsAdapter
                                .getFragment(i);
                        if (mgr != null) {
                            String tabname = mgr.getName(MainActivity2.this);
                            mTabsAdapter.setTabText(tab, tabname);
                            if (reload) {
                                mgr.reload();
                            }
                        }
                    }
                } else {
                    Fragment f = getSupportFragmentManager().findFragmentById(
                            R.id.container);
                    if (f != null && f instanceof ContentManager) {
                        ContentManager mgr = (ContentManager) f;
                        if (mgr != null) {
                            String tabname = mgr.getName(MainActivity2.this);
                            setTabText(getSupportActionBar().getSelectedTab(),
                                    tabname);
                            if (reload) {
                                mgr.reload();
                            }
                        }
                    }
                }
            } finally {
                mPref.setResumeReloadFlag(false);
            }
        }
    };

}
