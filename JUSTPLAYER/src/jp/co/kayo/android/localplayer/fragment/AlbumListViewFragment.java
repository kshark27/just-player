package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.AlbumartActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.AlbumListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

@SuppressLint("NewApi")
public class AlbumListViewFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment, LoaderCallbacks<Cursor>,
        OnScrollListener {
    private ViewCache viewcache;
    private final String SORTKEY = "albumview.sort";

    MyPreferenceManager mPref;
    AlbumListViewAdapter mAdapter;
    String sort_cname;
    private String mQueryString;
    private ListView mListView;
    private AnActionModeOfEpicProportions mActionMode;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.albumart_list_view, container,
                false);

        mListView = (ListView) root.findViewById(android.R.id.list);
        mListView.setOnScrollListener(this);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_albumart))
                    .setIcon(R.drawable.ic_menu_gallery)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            // 文字化け修正メニューを追加
            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }
    };

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_RATING: {
                ArrayList<Long> ids = new ArrayList<Long>();
                for (int i = 0; i < items.size(); i++) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(items
                            .get(i));
                    if (selectedCursor != null) {
                        long mediaId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(AudioAlbum._ID));
                        if (mediaId >= 0) {
                            ids.add(mediaId);
                        }
                    }
                }

                RatingDialog dlg = new RatingDialog(getActivity(),
                        TableConsts.FAVORITE_TYPE_ALBUM,
                        ids.toArray(new Long[ids.size()]), mHandler);
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_ADD: {
                ArrayList<String> whereArgs = new ArrayList<String>();
                StringBuilder where = new StringBuilder();
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        String albumKey = selectedCursor
                                .getString(selectedCursor
                                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(AudioMedia.ALBUM_KEY + " = ?");
                        whereArgs.add(albumKey);
                    }
                }

                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(
                        getActivity(), getFragmentManager(), where.toString(),
                        whereArgs.toArray(new String[whereArgs.size()]),
                        AudioMedia.ALBUM_KEY + "," + AudioMedia.TRACK + ","
                                + AudioMedia.DATA);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                Cursor cursor = null;
                try {
                    for (Integer i : items) {
                        Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                        if (selectedCursor != null) {
                            String albumKey = selectedCursor
                                    .getString(selectedCursor
                                            .getColumnIndex(AudioAlbum.ALBUM_KEY));
                            long albumId = selectedCursor
                                    .getLong(selectedCursor
                                            .getColumnIndex(AudioAlbum._ID));

                            ContentsUtils.clearAlbumCache(getActivity(),
                                    albumKey);

                            ContentValues values = new ContentValues();
                            values.put(AudioAlbum.ALBUM_INIT_FLG, 0);
                            getActivity().getContentResolver().update(
                                    ContentUris.withAppendedId(
                                            MediaConsts.ALBUM_CONTENT_URI,
                                            albumId), values, null, null);
                        }
                    }

                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                    reload();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            default: {
                messageHandle(what, items.get(0));
            }
            }
        }
    }

    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioAlbum._ID));
            String albumKey = selectedCursor.getString(selectedCursor
                    .getColumnIndex(AudioAlbum.ALBUM_KEY));

            switch (what) {
            case SystemConsts.EVT_SELECT_ARTIST: {
                String artist = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioAlbum.ARTIST));

                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.add(getId(),
                        ArtistListFragment.createFragment(null, artist, -1,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
                break;
            case SystemConsts.EVT_SELECT_MORE: {
                String artist = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioAlbum.ARTIST));
                if (artist != null) {
                    showInfoDialog(null, artist, null);
                }
            }
                break;
            case SystemConsts.EVT_SELECT_ALBUMART: {
                Intent i = new Intent(getActivity(), AlbumartActivity.class);
                i.putExtra("album_key", albumKey);
                getActivity().startActivity(i);
            }
                break;
            case SystemConsts.EVT_SELECT_EDIT: {
                Intent intent = new Intent(getActivity(), TagEditActivity.class);
                intent.putExtra(SystemConsts.KEY_EDITTYPE,
                        TagEditActivity.ALBUM);
                intent.putExtra(SystemConsts.KEY_EDITKEY, albumKey);
                getActivity().startActivityForResult(intent,
                        SystemConsts.REQUEST_TAGEDIT);
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            Cursor cursor = (Cursor) getListAdapter().getItem(position);
            if (cursor != null) {
                String album_key = cursor.getString(cursor
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
    
                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.add(getId(),
                        AlbumSongsFragment.createFragment(album_key,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public String selectSort() {
        int sort = mPref.getInteger(SORTKEY, 1);
        sort++;

        if (sort > 2) {
            sort = 0;
        }
        mPref.putInt(SORTKEY, sort);
        mPref.commit();
        getLoaderManager().restartLoader(getFragmentId(), null, this);

        return getName(getActivity());
    }

    @Override
    public int getFragmentId() {
        return R.layout.albumart_list_view;
    }

    private String[] getSort() {
        int sort = mPref.getInteger(SORTKEY, 1);
        String sortOrder = null;
        String cname = MediaConsts.AudioAlbum.ALBUM;
        if (sort == 0) {
            sortOrder = MediaConsts.AudioAlbum.ALBUM;
        } else if (sort == 1) {
            sortOrder = MediaConsts.AudioAlbum.ARTIST + ","
                    + MediaConsts.AudioAlbum.FIRST_YEAR;
            sortOrder = sortOrder + "," + MediaConsts.AudioAlbum.ALBUM;
            cname = MediaConsts.AudioAlbum.ARTIST;
        } else if (sort == 2) {
            sortOrder = MediaConsts.AudioAlbum.FIRST_YEAR;
        }

        return new String[] { cname, sortOrder };
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String[] sortname = getSort();
        if (mAdapter == null) {
            mAdapter = new AlbumListViewAdapter(getActivity(), null, viewcache,
                    sortname[0]);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        if (Funcs.isNotEmpty(mQueryString)) {
            StringBuilder buf = new StringBuilder();
            buf.append("( ").append(
                    MediaConsts.AudioAlbum.ALBUM
                            + " like '%' || ? || '%' ) OR (");
            buf.append(MediaConsts.AudioAlbum.ARTIST
                    + " like '%' || ? || '%' )");
            selection = buf.toString();
            selectionArgs = new String[] { mQueryString, mQueryString };
        }

        sort_cname = sortname[0];
        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.ALBUM_CONTENT_URI,
                null, selection, selectionArgs, sortname[1]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.swapCursor(null);
                    invisibleProgressBar();
                    return;
                }
                mAdapter.setCname(sort_cname);
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    if (data != null && !data.isClosed()) {
                        int count = data.getCount();
                        try {
                            mcur = new MatrixCursor(data.getColumnNames(),
                                    data.getCount());
                            if (count > 0) {
                                data.moveToFirst();
                                do {
                                    ArrayList<Object> values = new ArrayList<Object>();
                                    for (int i = 0; i < mcur.getColumnCount(); i++) {
                                        if (AudioAlbum.NUMBER_OF_SONGS
                                                .equals(mcur.getColumnName(i))) {
                                            values.add(data.getInt(i));
                                        } else {
                                            values.add(data.getString(i));
                                        }
                                    }
                                    mcur.addRow(values
                                            .toArray(new Object[values.size()]));
                                } while (data.moveToNext());
                                Cursor cur = mAdapter.swapCursor(mcur);
                            }
                        } finally {
                            if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                invisibleProgressBar();
                            }
                            if (data != null) {
                                data.close();
                            }
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    public static final String[] fetchcols = new String[] { AudioAlbum._ID,
            AudioAlbum.ALBUM, AudioAlbum.ALBUM_KEY, AudioAlbum.ALBUM_ART,
            AudioAlbum.FIRST_YEAR, AudioAlbum.LAST_YEAR,
            AudioAlbum.NUMBER_OF_SONGS, AudioAlbum.ARTIST,
            AudioAlbum.DATE_ADDED, AudioAlbum.DATE_MODIFIED };

    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.ALBUM_CONTENT_URI,
                                                fetchcols,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current) },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioAlbum.NUMBER_OF_SONGS
                                                        .equals(colname)) {
                                                    values.add(cursor
                                                            .getInt(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_albums_name);
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString == null
                || !mQueryString.equals(queryString);
        if (dirty) {
            mQueryString = queryString;
            reload();
        }
    }
}
