package jp.co.kayo.android.localplayer.fragment;

import java.util.ArrayList;
import java.util.List;

import com.commonsware.cwac.tlv.TouchListView;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.PlaylistSongsAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.ProgressFragment;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.ProgressTask;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistMemberInfo;

public class PlaylistEditFragment extends BaseListFragment implements ContentManager,
        ContextMenuFragment, LoaderCallbacks<List<PlaylistMemberInfo>>, OnClickListener {
    MyPreferenceManager mPref;
    ViewCache mViewCache;
    EditText editPlaylistName;
    TouchListView mListView;
    PlaylistSongsAdapter mAdapter;
    private AnActionModeOfEpicProportions mActionMode;
    long mPlaylistId;
    String mPlaylistName;

    ViewCache getCache() {
        if (mViewCache == null) {
            mViewCache = (ViewCache) getFragmentManager().findFragmentByTag(
                    SystemConsts.TAG_CACHE);
        }
        return mViewCache;
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    public static PlaylistEditFragment createFragment(long playlistId, String name, Bundle args) {
        PlaylistEditFragment f = new PlaylistEditFragment();
        args.putLong("playlistId", playlistId);
        args.putString("playlistName", name);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mPref == null) {
            mPref = new MyPreferenceManager(getActivity());
        }

        Bundle args = getArguments();
        if (args != null) {
            mPlaylistId = args.getLong("playlistId");
            mPlaylistName = args.getString("playlistName");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.playlist_edit_view, null);

        mListView = (TouchListView) root.findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        editPlaylistName = (EditText) root.findViewById(R.id.editPlaylistName);
        editPlaylistName.setText(mPlaylistName);
        editPlaylistName.setSelection(mPlaylistName.length());
        mListView.setDropListener(onDrop);
        mListView.setDragListener(onDrag);

        root.findViewById(R.id.buttonCancel).setOnClickListener(this);
        root.findViewById(R.id.buttonSubmit).setOnClickListener(this);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(getActivity().getCurrentFocus()!=null){
            InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);             
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
        
        ControlFragment control = (ControlFragment) getFragmentManager()
                .findFragmentByTag(SystemConsts.TAG_CONTROL);
        if (control != null) {
            control.showControl(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ControlFragment control = (ControlFragment) getFragmentManager()
                .findFragmentByTag(SystemConsts.TAG_CONTROL);
        if (control != null) {
            control.hideControl(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.buttonCancel) {
            hideMenu();
            BaseActivity base = (BaseActivity) getActivity();
            base.getSupportFragmentManager().popBackStack();
            mPref.setResumeReloadFlag(true);
        } else if (v.getId() == R.id.buttonSubmit) {
            hideMenu();
            final BaseActivity base = (BaseActivity) getActivity();

            // name
            String name = editPlaylistName.getText().toString();
            if (!mPlaylistName.equals(name)) {
                ContentValues values = new ContentValues();
                values.put(AudioPlaylist.NAME, name);
                base.getContentResolver().update(MediaConsts.PLAYLIST_CONTENT_URI, values,
                        AudioPlaylist._ID + " = ?", new String[] {
                            Long.toString(mPlaylistId)
                        });
                mPref.setResumeReloadFlag(true);
            }

            // commit
            final IMediaPlayerService binder = getBinder();
            ProgressTask progTask = new ProgressTask(getFragmentManager()) {
                @Override
                protected Void doInBackground(Void... params) {
                    numberingPlaylist(base, binder, this.dialog);
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                    base.getSupportFragmentManager().popBackStack();
                }
            };
            progTask.execute();
        }
    }

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
                case SystemConsts.EVT_SELECT_DEL: {
                    final ArrayList<String> whereArgs = new ArrayList<String>();
                    final StringBuilder where = new StringBuilder();
                    for (Integer i : items) {
                        PlaylistMemberInfo item = mAdapter.getItem(i);
                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(AudioMedia._ID + " = ?");
                        whereArgs.add(Long.toString(item.id));
                    }

                    final Uri playlisturi = ContentUris.withAppendedId(
                            MediaConsts.PLAYLIST_CONTENT_URI, mPlaylistId);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getString(R.string.lb_confirm));
                    builder.setMessage(String.format(
                            getString(R.string.fmt_remove_orderlist),
                            Integer.toString(items.size())));
                    builder.setPositiveButton(getString(R.string.lb_ok),
                            new android.content.DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // delte proc
                                    getActivity().getContentResolver().delete(playlisturi,
                                            where.toString(),
                                            whereArgs.toArray(new String[whereArgs.size()]));
                                    reload();
                                }
                            });
                    builder.setNegativeButton(getString(R.string.lb_cancel), null);
                    builder.setCancelable(true);
                    AlertDialog dlg = builder.create();
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_CHECKALL: {
                    for (int i = 0; i < mListView.getCount(); i++) {
                        mListView.setItemChecked(i, true);
                    }
                }
                    break;

            }
        }
    }

    @Override
    public void release() {

    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }

    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_playlist_edit);
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        getFragmentManager().popBackStack();
        return null;
    }

    @Override
    public void doSearchQuery(String queryString) {
    }

    @Override
    public int getFragmentId() {
        return R.layout.playlist_edit_view;
    }

    private void numberingPlaylist(Context context, IMediaPlayerService binder,
            ProgressFragment dialog) {
        Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, mPlaylistId);

        // member
        int size = mAdapter.getCount();
        // All Delete
        context.getContentResolver().delete(playlisturi, null, null);
        // All Insert
        dialog.setMax(size);
        for (int i = 0; i < size; i++) {
            dialog.setProgress(i);
            PlaylistMemberInfo info = mAdapter.getItem(i);
            long id = info.mediaId;
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, id);
            values.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, mPlaylistId);
            values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, i + 1);
            context.getContentResolver().insert(playlisturi, values);
        }
        reload();
    }

    @Override
    public Loader<List<PlaylistMemberInfo>> onCreateLoader(int arg0, Bundle arg1) {

        OrderInfoLoader loader = new OrderInfoLoader(getActivity(), mPlaylistId);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PlaylistMemberInfo>> arg0, List<PlaylistMemberInfo> arg1) {
        mAdapter = new PlaylistSongsAdapter(getActivity(), arg1, getCache());
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<PlaylistMemberInfo>> arg0) {
        mAdapter = new PlaylistSongsAdapter(getActivity(), new ArrayList<PlaylistMemberInfo>(),
                getCache());
        mListView.setAdapter(mAdapter);
    }

    private TouchListView.DropListener onDrop = new TouchListView.DropListener() {
        @Override
        public void drop(int from, int to) {
            Logger.d("Drop " + from + " to " + to);
            if (mActionMode.getCheckedItemCount() == 0) {
                PlaylistMemberInfo item = mAdapter.getItem(from);
                item.trackTo = to;
                mAdapter.remove(item);
                mAdapter.insert(item, to);
            }
        }
    };

    private TouchListView.DragListener onDrag = new TouchListView.DragListener() {

        @Override
        public void drag(int from, int to) {
            Logger.d("Drop " + from + " to " + to);
        }
    };

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_remove)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            return true;
        }
    }

    private static class OrderInfoLoader extends AsyncTaskLoader<List<PlaylistMemberInfo>> {
        private long playlistId;

        public OrderInfoLoader(Context context, long playlistId) {
            super(context);
            this.playlistId = playlistId;
        }

        @Override
        public List<PlaylistMemberInfo> loadInBackground() {
            ArrayList<PlaylistMemberInfo> list = new ArrayList<PlaylistMemberInfo>();

            Uri uri = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);

            Cursor cursor = null;
            try {
                cursor = getContext().getContentResolver().query(uri, null, null, null,
                        AudioPlaylistMember.PLAY_ORDER);
                int index = 0;
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        int audioIdCol = cursor.getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
                        if(audioIdCol==-1){
                            audioIdCol = cursor.getColumnIndex(MediaConsts.Media._ID);
                        }
                        PlaylistMemberInfo info = new PlaylistMemberInfo();
                        info.id = cursor.getLong(cursor
                                .getColumnIndex(MediaConsts.AudioPlaylistMember._ID));
                        info.mediaId = cursor.getLong(audioIdCol);
                        info.track = index++;
                        info.trackTo = info.track;
                        info.title = cursor.getString(cursor
                                .getColumnIndex(MediaConsts.AudioMedia.TITLE));
                        info.artist = cursor.getString(cursor
                                .getColumnIndex(MediaConsts.AudioMedia.ARTIST));
                        info.duration = cursor.getLong(cursor
                                .getColumnIndex(MediaConsts.AudioMedia.DURATION));
                        list.add(info);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            return list;
        }

    }
}
