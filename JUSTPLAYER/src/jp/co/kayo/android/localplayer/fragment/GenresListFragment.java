package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.GenresListAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenresMember;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;

public class GenresListFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment,
        LoaderCallbacks<Cursor>, OnScrollListener, IProgressView {
    private String genres_id;
    private ViewCache viewcache;
    MyPreferenceManager mPref;
    private GenresListAdapter mAdapter;
    String genrestitle;
    boolean getall = false;
    MatrixCursor mcur;
    long lastQueryTime = 0;
    ListView mListView;
    private AnActionModeOfEpicProportions mActionMode;
    Runnable mTask = null;
    private int mLoaderId = -1;
    boolean isReadFooter;

    private String getAudioIdCol(Cursor cursor) {
        String ret = MediaConsts.AudioGenresMember._ID;
        int col = cursor.getColumnIndex(MediaConsts.AudioGenresMember.AUDIO_ID);
        if (col != -1) {
            ret = MediaConsts.AudioGenresMember.AUDIO_ID;
        }
        return ret;
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    public static GenresListFragment createFragment(String selectedkey, String selectedtitle,
            Bundle args) {
        GenresListFragment f = new GenresListFragment();
        args.putString("selectedkey", selectedkey);
        args.putString("selectedtitle", selectedtitle);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            CharSequence id = args.getCharSequence("selectedkey");
            if (id != null) {
                genres_id = id.toString();
            }
            CharSequence title = args.getCharSequence("selectedtitle");
            if (title != null) {
                genrestitle = title.toString();
            }
        }
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
        mPref = new MyPreferenceManager(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.genres_list_view, container,
                false);

        mListView = (ListView) root
                .findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
        mLoaderId = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_album))
                    .setIcon(R.drawable.ic_menu_album)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.useLastFM();
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }

    };

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
                case SystemConsts.EVT_SELECT_ADD: {
                    long[] ids = new long[items.size()];
                    for (int i = 0; i < items.size(); i++) {
                        int index = items.get(i);
                        Cursor selectedCursor = (Cursor) mAdapter.getItem(index);
                        if (selectedCursor != null) {
                            long mediaId = selectedCursor.getLong(selectedCursor
                                    .getColumnIndex(AudioMedia._ID));
                            ids[i] = mediaId;
                        }
                    }

                    AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                            getFragmentManager(), ids);
                    task.execute();
                }
                    break;
                case SystemConsts.EVT_SELECT_CLEARCACHE: {
                    Cursor cursor = null;
                    try {
                        for (Integer i : items) {
                            Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                            if (selectedCursor != null) {
                                long mediaId = selectedCursor.getLong(selectedCursor
                                        .getColumnIndex(AudioMedia._ID));
                                String data = selectedCursor.getString(selectedCursor
                                        .getColumnIndex(MediaConsts.AudioMedia.DATA));

                                ContentsUtils.clearMediaCache(getActivity(), data);
                                ContentValues values = new ContentValues();
                                values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                                getActivity().getContentResolver().update(
                                        ContentUris.withAppendedId(
                                                MediaConsts.MEDIA_CONTENT_URI, mediaId),
                                        values, null, null);
                            }
                        }
                        datasetChanged();
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                        reload();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_LOVE:
                case SystemConsts.EVT_SELECT_BAN: {
                    for (Integer i : items) {
                        messageHandle(what, i);
                    }
                }
                    break;
                default: {
                    messageHandle(what, items.get(0));
                }
            }
        }
    }

    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioMedia._ID));
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
            String album = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ALBUM));
            String artist = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ARTIST));
            long duration = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DURATION));

            switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    RatingDialog dlg = new RatingDialog(getActivity(),
                            TableConsts.FAVORITE_TYPE_SONG, mediaId, mHandler);
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_ARTIST: {
                    if (artist != null) {
                        FragmentTransaction t = getFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(mPref, t);
                        t.replace(getId(),
                                jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                        .createFragment(null, artist, -1,
                                                FragmentUtils.cloneBundle(this)));
                        t.addToBackStack(SystemConsts.TAG_SUBCHILDFRAGMENT);
                        t.hide(this);
                        t.commit();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_OPENALBUM: {
                    Hashtable<String, String> tbl = ContentsUtils.getMedia(
                            getActivity(), new String[] {
                                AudioMedia.ALBUM_KEY
                            },
                            mediaId);
                    String album_key = tbl.get(AudioMedia.ALBUM_KEY);
                    if (album_key != null) {
                        FragmentTransaction t = getFragmentManager().beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(mPref, t);
                        t.replace(getId(),
                                AlbumSongsFragment.createFragment(album_key,
                                        FragmentUtils.cloneBundle(this)));
                        t.addToBackStack(SystemConsts.TAG_SUBCHILDFRAGMENT);
                        t.hide(this);
                        t.commit();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    showInfoDialog(album, artist, title);
                }
                    break;
                case SystemConsts.EVT_SELECT_LOVE: {
                    ContentsUtils.lastfmLove(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_BAN: {
                    ContentsUtils.lastfmBan(getActivity(), title, artist, album,
                            Long.toString(duration));
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    // 文字化け修正
                    Intent intent = new Intent(getActivity(), TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.MEDIA);
                    intent.putExtra(SystemConsts.KEY_EDITKEY,
                            Long.toString(mediaId));
                    getActivity().startActivityForResult(intent, SystemConsts.REQUEST_TAGEDIT);
                }
                    break;
                case SystemConsts.EVT_SELECT_CHECKALL: {
                    for (int i = 0; i < mListView.getCount(); i++) {
                        mListView.setItemChecked(i, true);
                    }
                }
                    break;
                default: {
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            Cursor cursor = (Cursor) mAdapter.getItem(position);
            BaseActivity base = (BaseActivity) getActivity();
            final IMediaPlayerService binder = base.getBinder();
            if (binder == null) {
                return;
            }
            final String cname1 = getAudioIdCol(cursor);
            final String cname2 = MediaConsts.AudioGenresMember.DATA;
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
    
                @Override
                protected Void doInBackground(Void... params) {
                    playMediaReplace(SystemConsts.CONTENTSKEY_GENRES + lastQueryTime, binder, cname1,
                            cname2, position);
                    return null;
                }
            };
            task.execute();
    
            if(mPref.isClickAndBack()){
                base.getSupportFragmentManager().popBackStack();
                mPref.setResumeReloadFlag(true);
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void release() {
    }

    public void play(MenuItem item) {
        ContentsUtils.playGenres(getBinder(), (BaseActivity) getActivity(),
                genres_id);
        Toast.makeText(getActivity(), getString(R.string.txt_action_addgenres),
                Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("NewApi")
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mAdapter == null) {
            mAdapter = new GenresListAdapter(getActivity(), null, viewcache);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }
        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(),
                MediaConsts.GENRES_MEMBER_CONTENT_URI, null,
                MediaConsts.AudioGenresMember.GENRE_ID + " = ?",
                new String[] {
                        genres_id
                }, null);
    }

    @SuppressLint("NewApi")
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        hideProgressBar();
        try {
            lastQueryTime = Calendar.getInstance().getTimeInMillis();
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    if (data != null && !data.isClosed()) {
                        try {
                            mcur = new MatrixCursor(data.getColumnNames(),
                                    data.getCount());
                            if (data.getCount() > 0) {
                                data.moveToFirst();
                                do {
                                    ArrayList<Object> values = new ArrayList<Object>();
                                    for (int i = 0; i < mcur.getColumnCount(); i++) {
                                        if (AudioGenresMember.DURATION
                                                .equals(mcur.getColumnName(i))) {
                                            values.add(data.getLong(i));
                                        } else {
                                            values.add(data.getString(i));
                                        }
                                    }
                                    mcur.addRow(values
                                            .toArray(new Object[values.size()]));
                                } while (data.moveToNext());
                                Cursor cur = mAdapter.swapCursor(mcur);
                            }
                        } finally {
                            if (data != null) {
                                data.close();
                            }
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    getall = true;
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            viewcache.setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public boolean playMediaReplace(String contentkey,
            IMediaPlayerService binder, String cname1, String cname2, int position) {
        try {
            if (!binder.setContentsKey(contentkey)) {
                // 既に設定中のリストだった
                int pos = binder.getPosition();
                if (pos != position) {// 同じ位置をクリックした
                    binder.lockUpdateToPlay();
                    try {
                        binder.reset();
                        binder.setPosition(position);
                    } finally {
                        binder.play();
                    }
                    return true;
                }
            } else {
                binder.addMediaQuery(MediaConsts.GENRES_MEMBER_CONTENT_URI.toString(), cname1,
                        cname2, MediaConsts.AudioGenresMember.GENRE_ID + " = ?", new String[] {
                            genres_id
                        }, null, position);
                return true;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    viewcache.progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    AsyncTask<Void, Void, Void> loadtask = null;

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                final BaseActivity activity = (BaseActivity) getActivity();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = activity
                                        .getContentResolver()
                                        .query(MediaConsts.GENRES_MEMBER_CONTENT_URI,
                                                null,
                                                MediaConsts.AudioGenresMember.GENRE_ID
                                                        + " = ? AND LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        genres_id,
                                                        Integer.toString(SystemConsts.DEFAULT_LIST_LIMIT),
                                                        Long.toString(current)
                                                },
                                                null);
                                if (mcur != null && cursor.getCount() > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioMedia.DURATION
                                                        .equals(colname)) {
                                                    values.add(cursor
                                                            .getLong(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());
                                } else {
                                    getall = true;
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        getFragmentManager().popBackStack();
        return null;
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_genre_songs);
    }

    @Override
    public void doSearchQuery(String queryString) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        if (mLoaderId == -1) {
            mLoaderId = new Random(System.currentTimeMillis()).nextInt(99999999);
        }
        return mLoaderId;
    }
}
