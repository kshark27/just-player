package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.AlbumartActivity;
import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseExpandListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.TreeItem;
import jp.co.kayo.android.localplayer.adapter.ArtistExpandViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IndexTreeCursorAdapter;
import jp.co.kayo.android.localplayer.dialog.AddPlaylistDialog;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleTreeChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.ProgressTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

public class ArtistExpandViewFragment extends BaseExpandListFragment implements
        ContentManager, ContextMenuFragment, LoaderCallbacks<Cursor>,
        OnScrollListener, OnChildClickListener, OnGroupClickListener {
    private ViewCache viewcache;
    private MyPreferenceManager mPref;
    private ExpandableListView mListView;
    ArtistExpandViewAdapter mAdapter;
    private AnActionModeOfEpicProportions mActionMode;
    int mSelectedPosition;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;
    private String mQueryString;

    @Override
    protected void messageHandle(int what, List<TreeItem> selectes) {
        if (selectes.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_ADD: {
                if (getActivity() != null) {
                    final BaseActivity base = (BaseActivity) getActivity();
                    final IMediaPlayerService binder = base.getBinder();
                    final ArrayList<TreeItem> items = new ArrayList<TreeItem>();
                    items.addAll(selectes);
                    ProgressTask progTask = new ProgressTask(
                            getFragmentManager()) {
                        List<Long> mediaList = new ArrayList<Long>();

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                binder.clearcut();
                                dialog.setMax(items.size());
                                ArrayList<Integer> addGroups = new ArrayList<Integer>();
                                for (int i = 0; i < items.size(); i++) {
                                    TreeItem item = items.get(i);
                                    if (item.childPosition != -1) {
                                        if (!addGroups
                                                .contains(item.groupPosition)) {
                                            Cursor selectedCursor = (Cursor) mAdapter
                                                    .getChild(
                                                            item.groupPosition,
                                                            item.childPosition);
                                            if (selectedCursor != null) {
                                                String albumKey = selectedCursor
                                                        .getString(selectedCursor
                                                                .getColumnIndex(AudioAlbum.ALBUM_KEY));
                                                addAlbums(base, binder,
                                                        albumKey, mediaList);
                                            }
                                        }
                                    } else {
                                        Cursor selectedCursor = (Cursor) mAdapter
                                                .getGroup(item.groupPosition);
                                        if (selectedCursor != null) {
                                            String artistKey = selectedCursor
                                                    .getString(selectedCursor
                                                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                                            addArtists(base, binder, artistKey,
                                                    mediaList);
                                            addGroups.add(item.groupPosition);
                                        }
                                    }

                                    dialog.setProgress(i);
                                }
                            } catch (RemoteException e) {
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            super.onPostExecute(result);
                            if (mediaList.size() > 0) {
                                long[] ids = new long[mediaList.size()];
                                for (int i = 0; i < mediaList.size(); i++) {
                                    ids[i] = mediaList.get(i);
                                }
                                AddPlaylistDialog dlg = new AddPlaylistDialog();
                                Bundle b = new Bundle();
                                b.putLongArray("playlist", ids);
                                b.putBoolean("hideOrderMenu", false);
                                dlg.setArguments(b);
                                dlg.show(fm, SystemConsts.TAG_RATING_DLG);
                            }
                        }

                    };
                    progTask.execute();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                if (getActivity() != null) {
                    final BaseActivity base = (BaseActivity) getActivity();
                    final IMediaPlayerService binder = base.getBinder();
                    final ArrayList<TreeItem> items = new ArrayList<TreeItem>();
                    items.addAll(selectes);
                    ProgressTask progTask = new ProgressTask(
                            getFragmentManager()) {
                        List<Long> mediaList = new ArrayList<Long>();

                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                binder.clearcut();
                                dialog.setMax(items.size());
                                ArrayList<Integer> addGroups = new ArrayList<Integer>();
                                for (int i = 0; i < items.size(); i++) {
                                    TreeItem item = items.get(i);
                                    if (item.childPosition != -1) {
                                        if (!addGroups
                                                .contains(item.groupPosition)) {
                                            Cursor selectedCursor = (Cursor) mAdapter
                                                    .getChild(
                                                            item.groupPosition,
                                                            item.childPosition);
                                            if (selectedCursor != null) {
                                                long albumId = selectedCursor
                                                        .getLong(selectedCursor
                                                                .getColumnIndex(AudioAlbum._ID));
                                                String albumKey = selectedCursor.getString(selectedCursor
                                                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
                                                ContentsUtils.clearAlbumCache(base, albumKey);
                                                ContentValues values = new ContentValues();
                                                values.put(
                                                        AudioAlbum.ALBUM_INIT_FLG,
                                                        0);
                                                base.getContentResolver()
                                                        .update(ContentUris
                                                                .withAppendedId(
                                                                        MediaConsts.ALBUM_CONTENT_URI,
                                                                        albumId),
                                                                values, null,
                                                                null);
                                            }
                                        }
                                    } else {
                                        Cursor selectedCursor = (Cursor) mAdapter
                                                .getGroup(item.groupPosition);
                                        if (selectedCursor != null) {
                                            String artistKey = selectedCursor
                                                    .getString(selectedCursor
                                                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                                            ContentsUtils.clearArtistCache(base,  artistKey);
                                        }
                                    }

                                    dialog.setProgress(i);
                                }
                            } catch (RemoteException e) {
                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void result) {
                            super.onPostExecute(result);
                            if (mediaList.size() > 0) {
                                long[] ids = new long[mediaList.size()];
                                for (int i = 0; i < mediaList.size(); i++) {
                                    ids[i] = mediaList.get(i);
                                }
                                AddPlaylistDialog dlg = new AddPlaylistDialog();
                                Bundle b = new Bundle();
                                b.putLongArray("playlist", ids);
                                b.putBoolean("hideOrderMenu", false);
                                dlg.setArguments(b);
                                dlg.show(fm, SystemConsts.TAG_RATING_DLG);
                            }
                        }

                    };
                    progTask.execute();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            default: {
                TreeItem item = selectes.get(0);
                messageHandle(what, item.groupPosition, item.childPosition);
            }
            }
        }
    }

    protected void addArtists(BaseActivity base, IMediaPlayerService binder,
            String artistKey, List<Long> mediaList) {
        Cursor cursor = null;
        try {
            cursor = getActivity().getContentResolver().query(
                    MediaConsts.MEDIA_CONTENT_URI,
                    new String[] { AudioMedia._ID },
                    AudioMedia.ARTIST_KEY + " = ?", new String[] { artistKey },
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                int n = cursor.getCount();
                if (n > 0) {
                    do {
                        long id = cursor.getLong(0);
                        mediaList.add(id);
                    } while (cursor.moveToNext());
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    protected void addAlbums(BaseActivity base, IMediaPlayerService binder,
            String albumKey, List<Long> mediaList) {
        Cursor cursor = null;
        try {
            cursor = getActivity().getContentResolver().query(
                    MediaConsts.MEDIA_CONTENT_URI,
                    new String[] { AudioMedia._ID },
                    AudioMedia.ALBUM_KEY + " = ?", new String[] { albumKey },
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                int n = cursor.getCount();
                if (n > 0) {
                    do {
                        long id = cursor.getLong(0);
                        mediaList.add(id);
                    } while (cursor.moveToNext());
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void messageHandle(int what, int selectedGroupPosition,
            int selectedChildPosition) {
        Cursor selectedCursor = null;
        if (selectedChildPosition != -1) {
            selectedCursor = mAdapter.getChild(selectedGroupPosition,
                    selectedChildPosition);
            if (selectedCursor != null) {
                long mediaId = selectedCursor.getLong(selectedCursor
                        .getColumnIndex(AudioAlbum._ID));
                String albumKey = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));

                switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    RatingDialog dlg = new RatingDialog(getActivity(),
                            TableConsts.FAVORITE_TYPE_ALBUM, mediaId, mHandler);
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_ARTIST: {
                    String artist = selectedCursor.getString(selectedCursor
                            .getColumnIndex(AudioAlbum.ARTIST));

                    getFragmentManager().popBackStack(
                            SystemConsts.TAG_SUBFRAGMENT,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.add(FragmentUtils.getLayoutId(getActivity(), this),
                            ArtistListFragment.createFragment(null, artist, -1,
                                    FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    String artist = selectedCursor.getString(selectedCursor
                            .getColumnIndex(AudioAlbum.ARTIST));
                    if (artist != null) {
                        showInfoDialog(null, artist, null);
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_ALBUMART: {
                    Intent i = new Intent(getActivity(), AlbumartActivity.class);
                    i.putExtra("album_key", albumKey);
                    getActivity().startActivity(i);
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    Intent intent = new Intent(getActivity(),
                            TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.ALBUM);
                    intent.putExtra(SystemConsts.KEY_EDITKEY, albumKey);
                    getActivity().startActivityForResult(intent,
                            SystemConsts.REQUEST_TAGEDIT);
                }
                    break;
                default: {
                }
                }
            }
        } else {
            selectedCursor = mAdapter.getGroup(selectedGroupPosition);
            if (selectedCursor != null) {
                long mediaId = selectedCursor.getLong(selectedCursor
                        .getColumnIndex(AudioArtist._ID));
                String artistKey = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioArtist.ARTIST_KEY));

                switch (what) {
                case SystemConsts.EVT_SELECT_RATING: {
                    RatingDialog dlg = new RatingDialog(getActivity(),
                            TableConsts.FAVORITE_TYPE_ARTIST, mediaId, mHandler);
                    dlg.show();
                }
                    break;
                case SystemConsts.EVT_SELECT_ARTIST: {
                    String artist = selectedCursor.getString(selectedCursor
                            .getColumnIndex(AudioArtist.ARTIST));
                    if (artist != null) {
                        getFragmentManager().popBackStack(
                                SystemConsts.TAG_SUBFRAGMENT,
                                FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        FragmentTransaction t = getFragmentManager()
                                .beginTransaction();
                        AnimationHelper.setFragmentToPlayBack(mPref, t);
                        t.add(getId(),
                                jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                        .createFragment(null, artist, -1,
                                                FragmentUtils.cloneBundle(this)));
                        t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                        t.hide(this);
                        t.commit();
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_MORE: {
                    String artist = selectedCursor.getString(selectedCursor
                            .getColumnIndex(AudioArtist.ARTIST));
                    if (artist != null) {
                        showInfoDialog(null, artist, null);
                    }
                }
                    break;
                case SystemConsts.EVT_SELECT_EDIT: {
                    Intent intent = new Intent(getActivity(),
                            TagEditActivity.class);
                    intent.putExtra(SystemConsts.KEY_EDITTYPE,
                            TagEditActivity.ARTIST);
                    intent.putExtra(SystemConsts.KEY_EDITKEY, artistKey);
                    getActivity().startActivityForResult(intent,
                            SystemConsts.REQUEST_TAGEDIT);

                }
                    break;
                default: {
                }
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        mPref = new MyPreferenceManager(getActivity());
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.artistart_expandlist_view,
                container, false);

        mListView = (ExpandableListView) root.findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnChildClickListener(this);
        mListView.setOnGroupClickListener(this);
        mListView.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleTreeChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ExpandableListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            // 文字化け修正メニューを追加
            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            return true;
        }

    };

    @Override
    public boolean onChildClick(ExpandableListView l, View v,
            int groupPosition, int childPosition, long id) {
        if (mActionMode.hasMenu()) {
            return mActionMode.onChildClick(l, v, groupPosition, childPosition,
                    id);
        } else {
            Cursor cursor = (Cursor) mAdapter.getChild(groupPosition,
                    childPosition);
            if (cursor != null) {
                String album_key = cursor.getString(cursor
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));

                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.add(getId(),
                        AlbumSongsFragment.createFragment(album_key,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
        }

        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView l, View v,
            int groupPosition, long id) {
        if (mActionMode.hasMenu()) {
            return mActionMode.onGroupClick(l, v, groupPosition, id);
        }
        
        int count = mAdapter.getChildrenCount(groupPosition);
        if(count == 0){
            Cursor selectedCursor = mAdapter.getGroup(groupPosition);
            if (selectedCursor != null) {
                String artist = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioArtist.ARTIST));
                if (artist != null) {
                    getFragmentManager().popBackStack(
                            SystemConsts.TAG_SUBFRAGMENT,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.add(getId(),
                            jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                    .createFragment(null, artist, -1,
                                            FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.artistart_expandlist_view;
    }

    public String getSortOrder() {
        return MediaConsts.AudioArtist.ARTIST;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = getSortOrder();
        if (mAdapter == null) {
            mAdapter = new ArtistExpandViewAdapter(getActivity(), null,
                    viewcache);
            getListView().setOnTouchListener(
                    new IndexTreeCursorAdapter.FastOnTouchListener(
                            new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            mAdapter.changeCursor(null);
        }

        if (Funcs.isNotEmpty(mQueryString)) {
            selection = MediaConsts.AudioMedia.ARTIST + " like '%' || ? || '%'";
            selectionArgs = new String[] { mQueryString };
        }

        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.ARTIST_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.changeCursor(null);
                    invisibleProgressBar();
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    int count = data.getCount();
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    if (AudioArtist.NUMBER_OF_ALBUMS
                                            .equals(mcur.getColumnName(i))
                                            || AudioArtist.NUMBER_OF_TRACKS
                                                    .equals(mcur
                                                            .getColumnName(i))) {
                                        values.add(data.getInt(i));
                                    } else {
                                        values.add(data.getString(i));
                                    }
                                }
                                mcur.addRow(values.toArray(new Object[values
                                        .size()]));
                            } while (data.moveToNext());
                            mAdapter.changeCursor(mcur);
                        }

                    } finally {
                        if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                            invisibleProgressBar();
                        }
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    mAdapter.changeCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    public static final String[] fetchcols = new String[] { AudioArtist._ID,
            AudioArtist.ARTIST, AudioArtist.ARTIST_KEY,
            AudioArtist.NUMBER_OF_ALBUMS, AudioArtist.NUMBER_OF_TRACKS };

    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getGroupCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.ARTIST_CONTENT_URI,
                                                fetchcols,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current) },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioArtist.NUMBER_OF_ALBUMS
                                                        .equals(colname)
                                                        || AudioArtist.NUMBER_OF_TRACKS
                                                                .equals(colname)) {
                                                    values.add(cursor
                                                            .getInt(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_artist_name);
    }

    @Override
    public void doSearchQuery(String queryString) {
        boolean dirty = mQueryString == null
                || !mQueryString.equals(queryString);
        if (dirty) {
            mQueryString = queryString;
            reload();
        }
    }

}
