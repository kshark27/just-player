package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.FileListViewAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileType;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.ProgressFragment;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.task.ProgressTask;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class FileListViewFragment extends BaseListFragment implements
        ContentManager, ContextMenuFragment,
        LoaderCallbacks<Cursor>, OnScrollListener {
    private ViewCache viewcache;
    private final String SORTKEY = "folderview.sort";
    MyPreferenceManager mPref;
    FileListViewAdapter mAdapter;
    ListView mListView;
    String sort_cname;
    private AnActionModeOfEpicProportions mActionMode;

    Runnable mTask = null;

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.albumart_list_view, container,
                false);

        mListView = (ListView) root
                .findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnScrollListener(this);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }
        
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            return true;
        }
    };

    @Override
    protected void messageHandle(int what, List<Integer> selectes) {
        if (selectes.size() > 0) {
            switch (what) {
                case SystemConsts.EVT_SELECT_ADD: {
                    if (getActivity() != null) {
                        final BaseActivity base = (BaseActivity) getActivity();
                        final IMediaPlayerService binder = base.getBinder();
                        final ArrayList<Integer> items = new ArrayList<Integer>();
                        items.addAll(selectes);
                        ProgressTask progTask = new ProgressTask(getFragmentManager()) {
                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    binder.clearcut();
                                    dialog.setMax(items.size());
                                    boolean addItem = false, isFirstPlay = false;
                                    for (int i = 0; i < items.size(); i++) {
                                        Cursor selectedCursor = (Cursor) mAdapter.getItem(items
                                                .get(i));
                                        if (selectedCursor != null) {
                                            String type = selectedCursor.getString(selectedCursor
                                                    .getColumnIndex(FileMedia.TYPE));
                                            final String data = selectedCursor
                                                    .getString(selectedCursor
                                                            .getColumnIndex(FileMedia.DATA));
                                            if (type.equals(FileType.FOLDER)
                                                    || type.equals(FileType.ZIP)) {
                                                List<FileItem> items = new ArrayList<FileItem>();
                                                getFileItems(base, data, items);
                                                if (items.size() > 0) {
                                                    addMediaFiles(base, binder, items);
                                                    addItem = true;
                                                }
                                            } else if (type.equals(FileType.AUDIO)) {
                                                String title = selectedCursor
                                                        .getString(selectedCursor
                                                                .getColumnIndex(AudioMedia.TITLE));
                                                FileItem item = new FileItem(type, title, data);
                                                List<FileItem> singleFileItems = new ArrayList<FileItem>();
                                                singleFileItems.add(item);

                                                addMediaFiles(base, binder, singleFileItems);
                                                addItem = true;
                                            }
                                        }
                                        dialog.setProgress(i);
                                        if (addItem && !isFirstPlay) {
                                            binder.play();
                                            isFirstPlay = true;
                                        }
                                    }
                                } catch (RemoteException e) {
                                }

                                return null;
                            }
                        };
                        progTask.execute();
                    }
                }
                    break;
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            final Cursor cursor = (Cursor) getListAdapter().getItem(position);
            if (cursor != null) {
                // Folderならさらに掘る、そうでないなら再生する
                final String type = cursor.getString(cursor
                        .getColumnIndex(FileMedia.TYPE));
                final String title = cursor.getString(cursor
                        .getColumnIndex(FileMedia.TITLE));
                final String data = cursor.getString(cursor
                        .getColumnIndex(FileMedia.DATA));
                if (type != null) {
                    // MEDIA
                    if (type.equals(FileType.AUDIO)
                            || type.equals(FileType.ZIPENTRY)) {
                        final BaseActivity base = (BaseActivity) getActivity();
                        final IMediaPlayerService binder = base.getBinder();
                        final List<FileItem> items = getFileItems();
                        AsyncTask<Void, Void, Void> animTask = new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {
                                playMediaReplace(base, binder, null, items, position - 1, data);
                                return null;
                            }
                        };
                        animTask.execute();
                    } else if (type.equals(FileType.VIDEO)) {
                        // VIDEO
                        if (data != null && data.length() > 0) {
                            String ext = "*";
                            int lastDot = data.lastIndexOf('.');
                            if (lastDot > 0) {
                                ext = data.substring(lastDot + 1);
                            }
                            String mime = "video/" + ext;
    
                            if (!ContentsUtils.isSDCard(mPref)) {
                                Cursor cur = null;
                                try {
                                    String geturl = StreamCacherServer
                                            .getContentUri(getActivity(), data);
                                    if (geturl != null) {
                                        Uri uri = Uri.parse(geturl);
                                        Intent intent = new Intent(
                                                Intent.ACTION_VIEW);
                                        intent.setDataAndType(uri, mime);
                                        startActivity(intent);
    
                                        // 動画再生前に音楽一時停止
                                        IMediaPlayerService binder = getBinder();
                                        if (binder != null) {
                                            int stat;
                                            try {
                                                stat = binder.stat();
                                                if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                                                    binder.pause();
                                                }
                                            } catch (RemoteException e) {
                                            }
                                        }
                                    } else {
                                        // URLの取得に失敗してエラー、データがおかしいおそれ
                                    }
                                } catch (Exception e) {
                                    String mes = String.format(
                                            getString(R.string.fmt_intent_error),
                                            mime);
                                    Toast.makeText(this.getActivity(), mes,
                                            Toast.LENGTH_SHORT).show();
                                } finally {
                                    if (cur != null) {
                                        cur.close();
                                    }
                                }
                            } else {
                                File file = new File(data);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), mime);
                                try {
                                    startActivity(intent);
    
                                    // 動画再生前に音楽一時停止
                                    IMediaPlayerService binder = getBinder();
                                    if (binder != null) {
                                        try {
                                            binder.pause();
                                        } catch (RemoteException e) {
                                        }
                                    }
                                } catch (Exception e) {
                                    String mes = String.format(
                                            getString(R.string.fmt_intent_error),
                                            mime);
                                    Toast.makeText(this.getActivity(), mes,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString("path", data);
                        getLoaderManager().restartLoader(getFragmentId(), bundle,
                                this);
                    }
                }
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
    }

    private class FileItem {
        String type;
        String title;
        String data;

        FileItem(String type, String title, String data) {
            this.type = type;
            this.title = title;
            this.data = data;
        }
    }

    private List<FileItem> getFileItems() {
        List<FileItem> result = new ArrayList<FileItem>();
        int size = mAdapter.getCount();
        String type;
        String title;
        String data;
        for (int i = 0; i < size; i++) {
            Cursor cursor = (Cursor) mAdapter.getItem(i);
            type = cursor.getString(cursor
                    .getColumnIndex(FileMedia.TYPE));
            if (type != null
                    && (type.equals(FileType.AUDIO) || type
                            .equals(FileType.ZIPENTRY))) {
                title = cursor.getString(cursor
                        .getColumnIndex(FileMedia.TITLE));
                data = cursor.getString(cursor
                        .getColumnIndex(FileMedia.DATA));
                result.add(new FileItem(type, title, data));
            }
        }

        return result;
    }

    private int getFileItems(Context context, String path, List<FileItem> result) {
        if ("..".equals(path)) {
            return 0;
        }
        String[] sortname = getSort();
        String selection = null;
        String[] selectionArgs = null;
        if (path != null) {
            selection = FileMedia.DATA + " = ? AND "+FileMedia.KEEPPATH+" = true";
            selectionArgs = new String[] {
                    path
            };
        }else{
            selection = FileMedia.KEEPPATH+" = true";
        }

        Cursor cursor = null;
        String type;
        String title;
        String data;
        int count = 0;
        try {
            cursor = context.getContentResolver().query(MediaConsts.FOLDER_CONTENT_URI, null,
                    selection, selectionArgs, sortname[1]);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    type = cursor.getString(cursor
                            .getColumnIndex(FileMedia.TYPE));
                    if (type != null) {
                        if (type.equals(FileType.AUDIO) || type
                                .equals(FileType.ZIPENTRY)) {
                            title = cursor.getString(cursor
                                    .getColumnIndex(FileMedia.TITLE));
                            data = cursor.getString(cursor
                                    .getColumnIndex(FileMedia.DATA));
                            result.add(new FileItem(type, title, data));
                        }
                        else if (type.equals(FileType.FOLDER)) {
                            data = cursor.getString(cursor
                                    .getColumnIndex(FileMedia.DATA));
                            count += getFileItems(context, data, result);
                        }
                    }
                    count++;
                } while (cursor.moveToNext() && count < 1000);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return count;
    }

    private boolean addMediaFiles(Context context, IMediaPlayerService binder, List<FileItem> items) {
        try {
            binder.setContentsKey(SystemConsts.CONTENTSKEY_FOLDER
                    + System.currentTimeMillis());
            MediaData[] ids = new MediaData[items.size()];
            for (int i = 0; i < items.size(); i++) {
                FileItem item = items.get(i);
                Cursor file_cur = null;
                try {
                    file_cur = ContentsUtils.getFileItemCursor(context, item.data, item.type);
                    if (file_cur != null && file_cur.moveToFirst()) {
                        long media_id = file_cur
                                .getLong(file_cur
                                        .getColumnIndex(AudioMedia._ID));
                        String data = file_cur
                                .getString(file_cur
                                        .getColumnIndex(AudioMedia.DATA));
                        if (media_id > 0) {
                            ids[i] = new MediaData(media_id, data);
                        } else {
                            String title = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.TITLE));
                            String album = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.ALBUM));
                            String artist = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.ARTIST));
                            if (title == null) {
                                title = item.title;
                            }
                            if (album == null) {
                                album = getString(R.string.txt_unknown_album);
                            }
                            if (artist == null) {
                                artist = getString(R.string.txt_unknown_artist);
                            }
                            ids[i] = new MediaData(0, media_id, MediaData.NOTPLAYED, 0, title,
                                    album,
                                    artist, data);
                        }
                    }
                } finally {
                    if (file_cur != null) {
                        file_cur.close();
                    }
                }
            }

            binder.addMediaData(ids);
            return true;
        } catch (RemoteException e) {
        }
        return false;
    }

    private boolean playMediaReplace(Context context, IMediaPlayerService binder,
            ProgressFragment dialog,
            List<FileItem> items, int position, String selectedData) {
        try {
            binder.setContentsKey(SystemConsts.CONTENTSKEY_FOLDER
                    + System.currentTimeMillis());

            float dt = items.size() / 50;
            MediaData[] ids = new MediaData[items.size()];
            for (int i = 0; i < items.size(); i++) {
                float pos = dt * i;
                if (dialog != null) {
                    dialog.setProgress((int) (50 + pos));
                }
                FileItem item = items.get(i);
                Cursor file_cur = null;
                try {
                    file_cur = ContentsUtils.getFileItemCursor(context, item.data, item.type);
                    if (file_cur != null && file_cur.moveToFirst()) {
                        long media_id = file_cur
                                .getLong(file_cur
                                        .getColumnIndex(AudioMedia._ID));
                        String data = file_cur
                                .getString(file_cur
                                        .getColumnIndex(AudioMedia.DATA));
                        if (media_id > 0) {
                            ids[i] = new MediaData(media_id, data);
                        } else {
                            String title = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.TITLE));
                            String album = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.ALBUM));
                            String artist = file_cur
                                    .getString(file_cur
                                            .getColumnIndex(AudioMedia.ARTIST));
                            if (title == null) {
                                title = item.title;
                            }
                            if (album == null) {
                                album = getString(R.string.txt_unknown_album);
                            }
                            if (artist == null) {
                                artist = getString(R.string.txt_unknown_artist);
                            }
                            ids[i] = new MediaData(0, media_id, MediaData.NOTPLAYED, 0, title,
                                    album,
                                    artist, data);
                            if (data.equals(selectedData)) {
                                position = i;
                            }
                        }
                    }
                } finally {
                    if (file_cur != null) {
                        file_cur.close();
                    }
                }
            }

            binder.playMediaData(ids, position);
            return true;
        } catch (RemoteException e) {
        }
        return false;
    }

    @Override
    public String selectSort() {
        int sort = mPref.getInteger(SORTKEY, 1);
        sort++;

        if (sort > 1) {
            sort = 0;
        }
        mPref.putInt(SORTKEY, sort);
        mPref.commit();
        getLoaderManager().restartLoader(getFragmentId(), null, this);

        return getName(getActivity());
    }

    @Override
    public int getFragmentId() {
        return R.layout.albumart_list_view;
    }

    private String[] getSort() {
        int sort = mPref.getInteger(SORTKEY, 1);
        String cname = FileMedia.TITLE;
        String sortOrder = FileMedia.TITLE + "," + FileMedia.TYPE;
        if (sort == 0) {
            sortOrder = FileMedia.DATE_MODIFIED + "," + FileMedia.TYPE;
        }

        return new String[] {
                cname, sortOrder
        };
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] sortname = getSort();
        if (mAdapter == null) {
            mAdapter = new FileListViewAdapter(getActivity(), null, viewcache,
                    sortname[0]);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }
        sort_cname = sortname[0];

        showProgressBar();
        String selection = null;
        String[] selectionArgs = null;
        if (args != null) {
            String path = args.getString("path");
            if (path != null) {
                selection = FileMedia.DATA + " = ?";
                selectionArgs = new String[] {
                        path
                };
            }
        }

        return new CursorLoader(getActivity(), MediaConsts.FOLDER_CONTENT_URI,
                null, selection, selectionArgs, sortname[1]);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Logger.d("in onLoadFinished");
        try {
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.swapCursor(null);
                    return;
                }
                mAdapter.setCname(sort_cname);
                Cursor cur = mAdapter.swapCursor(data);
            }
        } finally {
            hideProgressBar();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // Logger.d("onScroll:"+firstVisibleItem+"/"+visibleItemCount+"/"+totalItemCount);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }

            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_folder_name);
    }

    @Override
    public void doSearchQuery(String queryString) {
    }
}
