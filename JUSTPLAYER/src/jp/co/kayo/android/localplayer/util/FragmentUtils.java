package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.fragment.AlbumGridViewFragment;
import jp.co.kayo.android.localplayer.fragment.AlbumListViewFragment;
import jp.co.kayo.android.localplayer.fragment.ArtistExpandViewFragment;
import jp.co.kayo.android.localplayer.fragment.ArtistListViewFragment;
import jp.co.kayo.android.localplayer.fragment.FileListViewFragment;
import jp.co.kayo.android.localplayer.fragment.GenresListViewFragment;
import jp.co.kayo.android.localplayer.fragment.MediaListViewFragment;
import jp.co.kayo.android.localplayer.fragment.PlaybackListViewFragment;
import jp.co.kayo.android.localplayer.fragment.PlaylistViewFragment;
import jp.co.kayo.android.localplayer.fragment.QuickHelpFragment;
import jp.co.kayo.android.localplayer.fragment.VideoListViewFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

public class FragmentUtils {
    public static Bundle cloneBundle(Fragment f){
        Bundle args = new Bundle();
        if(f.getArguments()!=null){
            args.putInt("tabposition", f.getArguments().getInt("tabposition"));
            args.putString("tagname", f.getArguments().getString("tagname"));
        }
        return args;
    }
    
    public static void saveTabKind(Context context, Fragment f, int kind){
        if(f!=null){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            Editor editor = pref.edit();
            editor.putInt("tag.kind"+getTabPosition(f), kind);
            editor.commit();
        }
    }
    
    public static int getTabKind(Context context, Fragment f){
        return getTabKind(context, getTabPosition(f));
    }
    
    public static int getTabKind(Context context, int pos){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if(pos >= 0){
            if(pos == 0){
                return pref.getInt("tag.kind"+pos, SystemConsts.TAG_ALBUM_GRID);
            }else if(pos == 1){
                return pref.getInt("tag.kind"+pos, SystemConsts.TAG_PLAYBACK);
            }else if(pos == 2){
                return pref.getInt("tag.kind"+pos, SystemConsts.TAG_PLAYLIST);
            }
            else{
                return pref.getInt("tag.kind"+pos, SystemConsts.TAG_BLANK);
            }
        }
        return -1;
    }
    
    public static Fragment createFragment(int kind){
        Fragment f = null;
        if (kind == SystemConsts.TAG_ALBUM_GRID) {
            f = new AlbumGridViewFragment();
        }
        else if (kind == SystemConsts.TAG_ALBUM_LIST) {
            f = new AlbumListViewFragment();
        }
        else if (kind == SystemConsts.TAG_ARTIST_LIST) {
            f = new ArtistListViewFragment();
        }
        else if (kind == SystemConsts.TAG_ARTIST_EXPAND) {
            f = new ArtistExpandViewFragment();
        }
        else if (kind == SystemConsts.TAG_MEDIA) {
            f = new MediaListViewFragment();
        }
        else if (kind == SystemConsts.TAG_PLAYLIST) {
            f = new PlaylistViewFragment();
        }
        else if (kind == SystemConsts.TAG_FOLDER) {
            f = new FileListViewFragment();
        }
        else if (kind == SystemConsts.TAG_GENRES) {
            f = new GenresListViewFragment();
        }
        else if (kind == SystemConsts.TAG_VIDEO) {
            f = new VideoListViewFragment();
        }
        else if (kind == SystemConsts.TAG_PLAYBACK) {
            f = new PlaybackListViewFragment();
        }
        else {
            f = new QuickHelpFragment();
        }
        return f;
    }
    
    public static String getDefaultTabName(Context context, int kind){
        if (kind == SystemConsts.TAG_ALBUM_GRID) {
            return context.getString(R.string.lb_tab_albums_name);
        }
        else if (kind == SystemConsts.TAG_ALBUM_LIST) {
            return context.getString(R.string.lb_tab_albums_name);
        }
        else if (kind == SystemConsts.TAG_ARTIST_LIST) {
            return context.getString(R.string.lb_tab_artist_name);
        }
        else if (kind == SystemConsts.TAG_ARTIST_EXPAND) {
            return context.getString(R.string.lb_tab_artist_name);
        }
        else if (kind == SystemConsts.TAG_MEDIA) {
            return context.getString(R.string.lb_tab_media_name);
        }
        else if (kind == SystemConsts.TAG_PLAYLIST) {
            return context.getString(R.string.lb_tab_playlist_name);
        }
        else if (kind == SystemConsts.TAG_FOLDER) {
            return context.getString(R.string.lb_tab_folder_name);
        }
        else if (kind == SystemConsts.TAG_GENRES) {
            return context.getString(R.string.lb_tab_genres_name);
        }
        else if (kind == SystemConsts.TAG_VIDEO) {
            return context.getString(R.string.lb_tab_videos_name);
        }
        else if (kind == SystemConsts.TAG_PLAYBACK) {
            return context.getString(R.string.lb_tab_order_name);
        }
        else {
            return context.getString(R.string.lb_tab_blank);
        }
    }
    
    public static int getTabPosition(Fragment f){
        Bundle args = f.getArguments();
        if(args!=null){
            return args.getInt("tabposition");
        }
        return -1; 
    }
    
    public static int getLayoutId(Context context, Fragment f){
        int pos = getTabPosition(f);
        if(pos !=-1){
            return context.getResources().getIdentifier("fragment_main"+(pos+1), "layout", "jp.co.kayo.android.localplayer");
        }
        else{
            return R.layout.fragment_main1;
        }
    }
    
    public static int getFragmentId(Context context, Fragment f){
        int pos = getTabPosition(f);
        if(pos !=-1){
            return context.getResources().getIdentifier("fragment_main"+(pos+1), "id", "jp.co.kayo.android.localplayer");
        }
        else{
            return R.id.fragment_main1;
        }
    }

}
