package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import jp.co.kayo.android.localplayer.R;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

/***
 * 効果音の再生を行う 効果音はあらかじめロードしておいてゲーム中はそれを使い回しします
 * 
 * @author yokmama
 * 
 */
public class SoundManager {
    // 利用する効果音を列挙する
    private static final int[] SoundList = { R.raw.cassette_off };
    // 効果音を鳴らすクラス
    private SoundPool mSoundPool;
    // ロードされたときに取得される効果音のIDとリソースのIDの紐付け
    private HashMap<Integer, Integer> sounds = new HashMap<Integer, Integer>();

    /***
     * 効果音の読み込み
     * 
     * @param context
     */
    public void init(Context context) {
        // SoundPoolの初期化
        mSoundPool = new SoundPool(SoundList.length, AudioManager.STREAM_MUSIC,
                0);
        // SoundPoolを使って効果音をロードし、戻り値のIDと効果音のリソースIDをペアにしてMapに登録
        for (int i = 0; i < SoundList.length; i++) {
            sounds.put(SoundList[i], mSoundPool.load(context, SoundList[i], 1));
        }
    }

    /***
     * 効果音の解放
     * 
     */
    public void release() {
        // 読み込まれていた効果音を解放します
        if (sounds != null) {
            Iterator<Entry<Integer, Integer>> ite = sounds.entrySet()
                    .iterator();
            for (; ite.hasNext();) {
                Entry<Integer, Integer> entry = ite.next();
                mSoundPool.unload(entry.getValue());
            }
        }
        if (mSoundPool != null) {
            mSoundPool.release();
            mSoundPool = null;
        }
    }

    /***
     * 効果音の再生 Rawの下に置いてあるリソースIDの名前で再生をします
     * 
     * @param res
     * @param vol
     */
    public void play(int res, float vol) {
        // 効果音の再生を再生する、ここではVolを左右同じにして再生しています。
        // 引数１:鳴らす効果音のID
        // 引数２:左のボリューム
        // 引数３:右のボリューム
        // 引数４　優先順位０が一番高い
        // 引数５　ループ回数（-1の場合は無限にループ、0の場合はループしない）
        // 引数６　再生速度（0.5〜2.0：1.0で通常の速度）
        mSoundPool.play(sounds.get(res), vol, vol, 0, 0, 1.0f);
    }
}
