package jp.co.kayo.android.localplayer.util.bean;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

public class SearchItem {
    public enum ITEM_TYPE {
        ALBUM, ARTIST, SONG, GROUP
    };

    public ITEM_TYPE type;
    public long id;
    public String albumKey;
    public String artistKey;
    public String title;
    public String album;
    public String artist;
    public long duration;
    public String data;
    public String art;
    public int number_of_tracks;
}
