package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;

public class ThemeHelper {
    public void selectTheme(Activity activity) {
        activity.setTheme(getTheme(activity));
    }

    public int getTheme(Context context) {
        MyPreferenceManager pref = new MyPreferenceManager(context);
        int type = pref.getThemeId();
        if (type == 0) {
            return R.style.Theme_JustPlayer_Dark;
        } else if (type == 1) {
            return R.style.Theme_JustPlayer_Light;
        } else if (type == 2) {
            return R.style.Theme_JustPlayer_Rie;
        } else if (type == 3) {
            return R.style.Theme_JustPlayer_Miku;
        } else {
            return R.style.Theme_JustPlayer_Miku;
        }
    }

    public boolean isInverseBackgroundForced(Context context) {
        int themeId = getTheme(context);
        if (themeId == R.style.Theme_JustPlayer_Light
                || themeId == R.style.Theme_JustPlayer_Miku) {
            return true;
        }
        return false;
    }
}
