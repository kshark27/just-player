package jp.co.kayo.android.localplayer.util;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;

import android.os.Environment;

public class SdCardAccessHelper {
    public static File cachedMusicDir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.mp3/");
    public static File cachedVideoDir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.video/");
    public static File cachedAlbumartDir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.albumart/");

    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode()) + ".dat";
            // Logger.d("fname = "+ fname);
            return fname;
        } else {
            return null;
        }
    }

    public static File getCacheFile(String uri) {
        String f = getFilename(uri);
        if (f != null) {
            File cacheFile = new File(cachedMusicDir, f);
            return cacheFile;
        }
        return null;
    }

    public static boolean existCachFile(String uri) {
        String f = getFilename(uri);
        if (f != null) {
            File cacheFile = new File(cachedMusicDir, f);
            if (cacheFile != null && cacheFile.exists()) {
                return true;
            }
        }
        return false;
    }

    /**
     * ディレクトリの容量を調査
     * 
     * @param dir
     * @return サイズ
     */
    public long calcDirSize(File dir) {
        long size = 0L;
        if (dir.isDirectory()) {
            File files[] = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    size += calcDirSize(f);
                }
            }
        } else {
            size = dir.length();
        }

        return size;
    }

    /**
     * 指定したディレクトリとその中身をすべて削除
     * 
     * @param dir
     */
    public void rmdir(File dir) {
        if (!dir.exists()) {
            return;
        }

        if (dir.isFile()) {
            dir.delete();
        }

        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    rmdir(f);
                }
                dir.delete();
            }
        }
    }
}
