package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import android.util.Log;

import de.vdheide.mp3.ID3;
import de.vdheide.mp3.ID3v2;
import de.vdheide.mp3.ID3v2Frame;

public class ID3Helper {
    static String TAG = "ID3Helper";

    public ID3Helper() {
    }

    public void conv(String[] files, String encode) {
        info("Using source encoding: " + encode);
        for (String file : files) {
            try {
                info("Converting " + file);
                convert(new File(file), encode);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    /***
     * TALB ALBUM TIT2 TITLE TCOM SINGER TPE1 ARTIST
     * 
     * @param file
     * @param map
     * @throws Exception
     */
    public void updateTag(File file, HashMap<String, String> map)
            throws Exception {
        boolean isDirty = false;
        id3v2 = new ID3v2(file);
        Vector vector = id3v2.getFrames();
        if (vector != null && vector.size() > 0) {
            Iterator iterator = vector.iterator();
            do {
                if (!iterator.hasNext())
                    break;
                ID3v2Frame id3v2frame = (ID3v2Frame) iterator.next();
                if (id3v2frame.getID().startsWith("T")) {
                    String value = map.get(id3v2frame.getID());
                    if (value != null) {
                        id3v2frame.setContent(value.getBytes());
                        isDirty = true;
                    }
                }
            } while (true);

            if (isDirty) {
                id3v2.touch();
                id3v2.update();
            }
        }
    }

    private static void info(String s) {
        if (!isInfo) {
            Log.i(TAG, s);
        }
    }

    private static void error(String s) {
        Log.e(TAG, s);
    }

    private void debug(String s) {
        if (isDebug) {
            Log.d(TAG, s);
        }
    }

    private void addFrame(String s, String s1) throws Exception {
        debug(s + ": " + s1);
        byte abyte0[] = s1.getBytes("UnicodeLittle");
        if (abyte0.length == 0) {
            return;
        } else {
            byte abyte1[] = new byte[abyte0.length + 3];
            System.arraycopy(abyte0, 0, abyte1, 1, abyte0.length);
            abyte1[abyte1.length - 2] = abyte1[abyte1.length - 1] = 0;
            abyte1[0] = 1;
            ID3v2Frame id3v2frame = new ID3v2Frame(s, abyte1, false, false,
                    false, (byte) 0, (byte) 0, (byte) 0);
            id3v2.addFrame(id3v2frame);
            return;
        }
    }

    private void convert(File file, String encode) throws Exception {
        id3 = new ID3(file);
        id3.encoding = encode;
        id3v2 = new ID3v2(file);
        boolean isCheckForTag = id3.checkForTag();
        boolean hasTag = id3v2.hasTag();
        // タグが存在する場合は処理をする（ただしV1強制の場合は不要)
        if (hasTag && !isForcev1) {
            try {
                id3v2.getFrames();
            } catch (Exception exception) {
                debug("Cannot get v2 frames, assuming no v2 tag.");
                hasTag = false;
            }
        }
        // タグがないか、あるいはV1で強制書き込みの場合に通る。基本的にはないと思われ
        if (isCheckForTag && !hasTag || isCheckForTag && isForcev1) {
            info("Converting id3v1 tag to id3v2 Unicode format.");
            if (hasTag && isForcev1) {
                info("Warning: v1 tag use forced, original v2 tag overwritten.");
            }
            id3v2.clear();
            addFrame("TALB", id3.getAlbum());// Album/Movie/Show title
            addFrame("TOPE", id3.getArtist());// Original artist(s)/performer(s)
            addFrame("TPE1", id3.getArtist());// Lead performer(s)/Soloist(s)]
            addFrame("COMM", id3.getComment());// Comments
            addFrame("TIT2", id3.getTitle());// Title/songname/content
                                             // description
            addFrame("TORY", id3.getYear());// Original release year
            addFrame("TYER", id3.getYear());// Year
            int i = id3.getGenre();
            if (i >= 0 && i < genreString.length)
                addFrame("TCON", genreString[i]);
            else
                addFrame("TCON", "unknown");
            addFrame("TRCK", id3.getTrack() + "");
            if (!isTest && isRemovev1)
                id3.removeTag();
            if (!isTest)
                id3v2.update();
        } else if (hasTag) {
            // メインになる処理
            info("Reencoding id3v2 tag into Unicode");
            boolean isDirty = false;
            Vector vector = id3v2.getFrames();
            if (vector != null && vector.size() > 0) {
                Iterator iterator = vector.iterator();
                do {
                    if (!iterator.hasNext())
                        break;
                    ID3v2Frame id3v2frame = (ID3v2Frame) iterator.next();
                    if (id3v2frame.getID().startsWith("T")) {
                        if (id3v2.getVersion() == 3
                                && NON_UNICODE_FIELDS.contains(id3v2frame
                                        .getID())) {
                            debug("No action for frame: " + id3v2frame.getID()
                                    + " because it's a v2.3 non-unicode field");
                        } else {
                            byte abyte0[] = id3v2frame.getContent();
                            if (abyte0.length > 1 && abyte0[0] == 0) {
                                String s1 = new String(abyte0, 1,
                                        abyte0.length - 1, encode);
                                debug(id3v2frame.getID() + ": " + s1);
                                byte abyte1[] = s1.getBytes("UnicodeLittle");
                                byte abyte2[] = new byte[abyte1.length + 5];
                                System.arraycopy(abyte1, 0, abyte2, 1,
                                        abyte1.length);
                                abyte2[abyte2.length - 2] = abyte2[abyte2.length - 1] = 0;
                                abyte2[0] = 1;
                                id3v2frame.setContent(abyte2);
                                isDirty = true;
                            }
                        }
                    } else {
                        debug("No action for frame: " + id3v2frame.getID());
                    }
                } while (true);
                if (!isTest && isDirty) {
                    id3v2.touch();
                    id3v2.update();
                }
                if (!isTest && isRemovev1)
                    id3.removeTag();
            }
        } else {
            error("File " + file.getAbsolutePath()
                    + " has no id3 tag, skipping!");
        }
    }

    public static boolean isDebug = false;
    public static boolean isRemovev1 = false;
    public static boolean isForcev1 = false;
    public static boolean isInfo = false;
    public static boolean isTest = false;
    private static HashSet NON_UNICODE_FIELDS;
    private static String _NON_UNICODE_FIELDS[] = { "TDAT", "TIME", "TPOS",
            "TRCK", "TYER" };
    ID3 id3;
    ID3v2 id3v2;
    String genreString[] = { "Blues", "Classic Rock", "Country", "Dance",
            "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age",
            "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno",
            "Industrial", "Alternative", "Ska", "Death Metal", "Pranks",
            "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal",
            "Jazz Funk", "Fusion", "Trance", "Classical", "Instrumental",
            "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise",
            "Alternative Rock", "Bass", "Soul", "Punk", "Space", "Meditative",
            "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic",
            "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk",
            "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta",
            "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American",
            "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes",
            "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka",
            "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk",
            "Folk/Rock", "National Folk", "Swing", "Fast Fusion", "Bebob",
            "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde",
            "Gothic Rock", "Progressive Rock", "Psychedelic Rock",
            "Symphonic Rock", "Slow Rock", "Big Band", "Chorus",
            "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson",
            "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass",
            "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango",
            "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul",
            "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A Capella",
            "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House",
            "Hardcore", "Terror", "Indie", "BritPop", "Negerpunk",
            "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal",
            "Black Metal", "Crossover", "Contemporary Christian",
            "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime",
            "JPop", "Synthpop" };

    static {
        NON_UNICODE_FIELDS = new HashSet();
        for (int i = 0; i < _NON_UNICODE_FIELDS.length; i++)
            NON_UNICODE_FIELDS.add(_NON_UNICODE_FIELDS[i]);

    }
}
