package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.util.ViewCache;
import android.content.Context;
import android.database.Cursor;

public abstract class ImageSkipCursorAdapter extends ProgressCursorAdapter {
    protected boolean imageloadskip = false;

    public ImageSkipCursorAdapter(Context context, Cursor c,
            Boolean autoRequery, ViewCache cache) {
        super(context, c, autoRequery, cache);
    }

    public void setImageLoadSkip(boolean b) {
        imageloadskip = b;
    }

}
