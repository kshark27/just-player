package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TabContentView extends FrameLayout {
    public TabContentView(Context context, String title, int rc) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getApplicationContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);

        View childview1 = inflater.inflate(R.layout.tabwidget1, null, false);

        ImageView imageView1 = (ImageView) childview1
                .findViewById(R.id.imageView1);
        TextView tabtext = (TextView) childview1.findViewById(R.id.tabtext);

        imageView1.setImageResource(rc);

        tabtext.setText(title);
        // tv1.setBackgroundResource(R.drawable.button);
        addView(childview1);

    }

}
