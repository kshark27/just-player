package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class ProgressDialogTask extends AsyncTask<Void, Integer, Void> {
    public Context context = null;
    public ProgressDialog dialog = null;
    public String msg;

    public ProgressDialogTask(Context context, String msg) {
        this.context = context;
        this.msg = msg;
    }

    public void udpate(Integer... progress) {
        this.publishProgress(progress);
    }

    @Override
    protected void onPreExecute() {
        // プログレスバー設定
        if (!ContentsUtils.isSDCard(context)) {
            dialog = new ProgressDialog(context);
            dialog.setIndeterminate(true);
            dialog.setMessage(msg);
            dialog.show();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (dialog != null) {
            dialog.setMessage(msg);
            dialog.setProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        if (dialog != null) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
            }
        }
    }
}
