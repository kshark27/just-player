package jp.co.kayo.android.localplayer.pref;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.List;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.DisplayPreference;
import jp.co.kayo.android.localplayer.appwidget.FontColorPreference;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ProviderInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class PremiumConfigPreference extends PreferenceActivity implements
        OnPreferenceChangeListener, OnPreferenceClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.premium_pref);
        getListView().setBackgroundColor(Color.TRANSPARENT);
        getListView().setCacheColorHint(Color.TRANSPARENT);

        Preference beam_pref = findPreference("key.beamConfig");
        Preference scrobbing = findPreference("key.useLastFM");
        Preference end_list = findPreference("key.useEndOfList");
        Preference font_pref = findPreference("key.fontcolor");
        Preference widget_pref = findPreference("key.widgetcolor");
        Preference exit_check = findPreference("key.exitcheck");
        Preference control_hide = findPreference("key.control_hide");
        Preference hide_art = findPreference("key.hideart");
        Preference useCacheServer = findPreference("key.useCacheServer");
        Preference  useControlNotification = findPreference("key.useControlNotification");
        Preference execmediascanner = findPreference("key.execmediascanner");

        beam_pref.setOnPreferenceClickListener(this);
        scrobbing.setOnPreferenceClickListener(this);
        end_list.setOnPreferenceClickListener(this);
        font_pref.setOnPreferenceClickListener(this);
        widget_pref.setOnPreferenceClickListener(this);
        exit_check.setOnPreferenceClickListener(this);
        control_hide.setOnPreferenceClickListener(this);
        hide_art.setOnPreferenceClickListener(this);
        useCacheServer.setOnPreferenceClickListener(this);
        execmediascanner.setOnPreferenceClickListener(this);

        useCacheServer.setOnPreferenceChangeListener(this);
        useCacheServer.setEnabled(true);
        
        if(Build.VERSION.SDK_INT >= 11){
            useControlNotification.setEnabled(false);
        }

        // // バージョンにおける設定
        if (Build.VERSION.SDK_INT >= 14) {
            //lock_pref.setEnabled(false);
            beam_pref.setEnabled(true);
            beam_pref.setOnPreferenceClickListener(this);
        } else {
            beam_pref.setEnabled(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Toast.makeText(this, getString(R.string.txt_pref_changed_notice),
                Toast.LENGTH_SHORT).show();
        
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if ("key.beamConfig".equals(preference.getKey())) {
            Intent i = new Intent(this, BeamConfigPreference.class);
            startActivity(i);
        } else if("key.fontcolor".equals(preference.getKey())) {
            Intent i = new Intent(this, FontColorPreference.class);
            startActivity(i);
        } else if ("key.widgetcolor".equals(preference.getKey())) {
            Intent i = new Intent(this, DisplayPreference.class);
            startActivity(i);
        } else if ("key.execmediascanner".equals(preference.getKey())) {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri
                    .parse("file://"
                            + Environment.getExternalStorageDirectory())));
            Toast.makeText(getApplicationContext(), getString(R.string.lb_callmediascanner), Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
