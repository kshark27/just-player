
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.service.RemoteControlClientCompat.MetadataEditorCompat;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.RemoteControlClient;
import android.os.Build;
import android.os.Handler;

@TargetApi(8)
public class AudioFocus {
    RemoteControlClientCompat mRemoteControlClientCompat;
    ComponentName mRemoteControlResponder;
    Context context;
    AudioManager audiomgr;
    Handler handler;

    private OnAudioFocusChangeListener mAudioFocusListener = new OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            Logger.d("onAudioFocusChange");
            handler.obtainMessage(MediaPlayerService.MSG_FOCUSCHANGE, focusChange, 0).sendToTarget();
        }
    };

    public AudioFocus(Context context, Handler handler, AudioManager audiomgr) {
        this.context = context;
        this.handler = handler;
        this.audiomgr = audiomgr;

        mRemoteControlResponder = new ComponentName(context.getPackageName(),
                RemoteControlReceiver.class.getName());
        audiomgr.registerMediaButtonEventReceiver(mRemoteControlResponder);
    }

    public RemoteControlClientCompat getRemoteControlClientCompat() {
        if (mRemoteControlClientCompat == null) {
            if (Build.VERSION.SDK_INT >= 14) {
                Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                intent.setComponent(mRemoteControlResponder);
                mRemoteControlClientCompat = new RemoteControlClientCompat(
                        PendingIntent.getBroadcast(context, 0,
                                intent, 0));
                RemoteControlHelper.registerRemoteControlClient(audiomgr,
                        mRemoteControlClientCompat);
            }
        }
        return mRemoteControlClientCompat;
    }

    public void unregister(AudioManager audiomgr) {
        if (mRemoteControlClientCompat != null) {
            mRemoteControlClientCompat
                    .setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
        }

        audiomgr.unregisterMediaButtonEventReceiver(mRemoteControlResponder);
        audiomgr.abandonAudioFocus(mAudioFocusListener);
    }

    public void register(Context context) {
        audiomgr.requestAudioFocus(mAudioFocusListener,
                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        // Update the remote controls
        RemoteControlClientCompat compat = getRemoteControlClientCompat();
        if (compat != null) {
            compat.setTransportControlFlags(
                    RemoteControlClient.FLAG_KEY_MEDIA_PLAY
                            | RemoteControlClient.FLAG_KEY_MEDIA_NEXT
                            | RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS
                            | RemoteControlClient.FLAG_KEY_MEDIA_STOP);
        }
    }

    public void setLockScreen(Context context, MyPreferenceManager pref, int stat,
            Hashtable<String, String> tbl1, Hashtable<String, String> tbl2, Bitmap bitmap) {
        boolean b = pref.useLockScreen();
        if (b) {
            RemoteControlClientCompat compat = getRemoteControlClientCompat();
            if (compat != null) {
                if ((stat & AppWidgetHelper.FLG_PLAY) != 0) {
                    try {
                        compat.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
                        MetadataEditorCompat editor = compat
                                .editMetadata(true)
                                .putString(MediaMetadataRetriever.METADATA_KEY_ARTIST,
                                        tbl1.get(AudioMedia.ARTIST))
                                .putString(MediaMetadataRetriever.METADATA_KEY_ALBUM,
                                        tbl1.get(AudioMedia.ALBUM))
                                .putString(MediaMetadataRetriever.METADATA_KEY_TITLE,
                                        tbl1.get(AudioMedia.TITLE))
                                .putLong(MediaMetadataRetriever.METADATA_KEY_DURATION,
                                        Funcs.parseLong(tbl1.get(AudioMedia.DURATION)));
                        if (bitmap != null) {
                            editor.putBitmap(
                                    RemoteControlClientCompat.MetadataEditorCompat.METADATA_KEY_ARTWORK,
                                    bitmap);
                        }
                        editor.apply();
                    } finally {
                    }
                }
                else {
                    compat.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
                }
            }

        }
    }
}
