
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.db.JustPlayerDatabaseHelper;
import jp.co.kayo.android.localplayer.fx.AudioFx;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.util.bean.MediaData;

public class JukeBox {
    public static final String PREF_DS = "pref.ds";
    private static final String PREF_POSITION = "pref.position";
    private static final String PREF_PLAYPOSITION = "pref.playposition";

    private GaplessMediaPlayer mp;
    private int repeatmode;
    private ArrayList<MediaData> playlist = new ArrayList<MediaData>();
    private ArrayList<Long> shuffle_list = new ArrayList<Long>();
    private boolean isShuffle;
    private int seekPosition = 0;
    private Random rand = new Random(System.nanoTime());
    private boolean isLoaded = false;
    private SharedPreferences pref;
    private Context mContext;
    private int position;
    private int seekposition = 0;
    private JustPlayerDatabaseHelper mDatabaseHelper = null;

    private Object dbLock = new Object();
    private SQLiteDatabase _wdb;
    private SQLiteDatabase _rdb;

    private SQLiteDatabase getWritableDatabase() {
        if (_wdb == null && mDatabaseHelper != null) {
            _wdb = mDatabaseHelper.getWritableDatabase();
        }
        return _wdb;
    }
    
    private SQLiteDatabase getReadableDatabase() {
        if (_rdb == null && mDatabaseHelper != null) {
            _rdb = mDatabaseHelper.getReadableDatabase();
        }
        return _rdb;
    }

    private void closeWritableDb() {
        if (_wdb != null) {
            _wdb.close();
            _wdb = null;
        }
    }
    
    private void closeReadableDb() {
        if (_rdb != null) {
            _rdb.close();
            _rdb = null;
        }
    }

    public JukeBox(Context context) {
        mContext = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        mDatabaseHelper = new JustPlayerDatabaseHelper(context);
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    private void loadPlaylist(SQLiteDatabase db, String cur_contenturi) {
        ArrayList<MediaData> tplaylist = new ArrayList<MediaData>();
        ArrayList<Long> tshufflelist = new ArrayList<Long>();

        Cursor cursor = null;
        try {
            cursor = db.query(
                    TableConsts.TBNAME_PLAYBACK,
                    new String[] {
                            BaseColumns._ID, TableConsts.PLAYBACK_MEDIA_ID,
                            TableConsts.PLAYBACK_STATE, TableConsts.PLAYBACK_DATA
                    },
                    TableConsts.PLAYBACK_URI + " = ?",
                    new String[] {
                        cur_contenturi
                    },
                    null,
                    null,
                    TableConsts.PLAYBACK_ORDER);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
                    long media_id = cursor.getInt(cursor
                            .getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID));
                    int state = cursor
                            .getInt(cursor.getColumnIndex(TableConsts.PLAYBACK_STATE));
                    String data = cursor.getString(cursor
                            .getColumnIndex(TableConsts.PLAYBACK_DATA));
                    if (state != MediaData.PLAYED && isShuffle) {
                        addShuffle(tshufflelist, id);
                    }
                    tplaylist.add(new MediaData(id, media_id, state, data));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        playlist = tplaylist;
        shuffle_list = tshufflelist;
    }

    public void load() {
        isLoaded = true;
        playlist = new ArrayList<MediaData>();
        shuffle_list = new ArrayList<Long>();
        isShuffle = pref.getBoolean(SystemConsts.PREF_SHUFFLEFLG, false);
        setRepeatMode(pref.getInt(SystemConsts.PREF_REPEATFLG, SystemConsts.FLG_REPEAT_NO));

        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);

        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getReadableDatabase();
                loadPlaylist(db, cur_contenturi);
            } finally {
            }
        }

        String save_contenturi = pref.getString(PREF_DS, "");
        if (save_contenturi.equals(cur_contenturi)) {
            position = pref.getInt(PREF_POSITION, -1);
            seekposition = pref.getInt(PREF_PLAYPOSITION, 0);
        } else {
            position = -1;
            seekposition = 0;
        }

        if (playlist.size() <= position) {
            position = -1;
            seekposition = 0;
        }
    }

    public void save(boolean prepareBefore) {
        mp.save();

        Editor editor = pref.edit();

        editor.putString(PREF_DS, pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY));
        editor.putInt(PREF_POSITION, position);
        if (!prepareBefore && mp != null && mp.isPlaying()) {
            seekposition = mp.getCurrentPosition();
        }
        else {
            seekposition = 0;
        }
        editor.putInt(PREF_PLAYPOSITION, seekposition);
        editor.putBoolean(SystemConsts.PREF_SHUFFLEFLG, isShuffle);
        editor.putInt(SystemConsts.PREF_REPEATFLG, getRepeatMode());

        editor.commit();
    }

    public int getSeekPosition() {
        return seekPosition;
    }

    public void setSeekPosition(int seekPosition) {
        this.seekPosition = seekPosition;
    }

    public boolean isShaffle() {
        return isShuffle;
    }

    public int getRepeatMode() {
        return repeatmode;
    }

    public void setRepeatMode(int mode) {
        this.repeatmode = mode;
    }

    private final String[] FETCH = new String[] {
            AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION
    };
    
    public boolean addMedia(MediaData[] items) {
        boolean updatelist = false;
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                for (MediaData item : items) {
                    updatelist = true;
                    if (item.mediaId > 0) {
                        addMedia(db, cur_contenturi, item.mediaId, item.data);
                    } else {
                        if (item.id == 0) {
                            item.id = System.nanoTime() * -1;
                        }
                        addMedia(db, item.id, MediaData.NOTPLAYED, item.getDuration(),
                                item.getTitle(),
                                item.getAlbum(), item.getArtist(), item.data);
                    }
                }
                db.setTransactionSuccessful();

                return updatelist;
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }
    }

    public boolean addMedia(MediaData[] items, int pos) {
        boolean updatelist = false;
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                for (MediaData item : items) {
                    updatelist = true;
                    if (item.mediaId > 0) {
                        addMedia(db, cur_contenturi, item.mediaId, item.data);
                    } else {
                        if (item.id == 0) {
                            item.id = System.nanoTime() * -1;
                        }
                        addMedia(db, item.id, MediaData.NOTPLAYED, item.getDuration(),
                                item.getTitle(),
                                item.getAlbum(), item.getArtist(), item.data);
                    }
                }
                setPosition(db, pos);
                db.setTransactionSuccessful();

                return updatelist;
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }
    }

    public boolean addMedia(Cursor cursor, String cname1, String cname2, int pos) {
        boolean updatelist = false;
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        int col1 = cursor.getColumnIndex(cname1);
        int col2 = cursor.getColumnIndex(cname2);
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                do {
                    updatelist = true;
                    long id = cursor.getLong(col1);
                    String data = cursor.getString(col2);
                    addMedia(db, cur_contenturi, id, data);
                    updatelist = true;
                } while (cursor.moveToNext());
                setPosition(db, pos);
                db.setTransactionSuccessful();
                return updatelist;
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }
    }

    public void addMedia(long id, int notplayed, long duration, String title,
            String album,
            String artist, String data) {
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                addMedia(db, id, MediaData.NOTPLAYED, duration, title, album, artist, data);
                db.setTransactionSuccessful();
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }
    }

    private void addMedia(SQLiteDatabase db, long id, int notplayed, long duration, String title,
            String album,
            String artist, String data) {

        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_URI, cur_contenturi);
        values.put(TableConsts.PLAYBACK_TITLE, title);
        values.put(TableConsts.PLAYBACK_ARTIST, artist);
        values.put(TableConsts.PLAYBACK_ALBUM, album);
        values.put(TableConsts.PLAYBACK_MEDIA_ID, id);
        // values.put(TableConsts.PLAYBACK_ALBUM_KEY, null);
        // values.put(TableConsts.PLAYBACK_ARTIST_KEY, null);
        values.put(TableConsts.PLAYBACK_DATA, data);
        values.put(TableConsts.PLAYBACK_DURATION, duration);
        values.put(TableConsts.PLAYBACK_ORDER, System.currentTimeMillis());
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);
        long createid = db.insert(TableConsts.TBNAME_PLAYBACK, null, values);
        playlist.add(new MediaData(createid, id, MediaData.NOTPLAYED, duration, title, album,
                artist, data));
        if (isShuffle) {
            addShuffle(shuffle_list, createid);
        }
    }

    public void addMedia(long id, String data) {
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        addMedia(cur_contenturi, id, data);
    }

    public void addMedia(String cur_contenturi, long id, String data) {
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_URI, cur_contenturi);
        values.put(TableConsts.PLAYBACK_MEDIA_ID, id);
        values.put(TableConsts.PLAYBACK_DATA, data);
        values.put(TableConsts.PLAYBACK_ORDER, System.currentTimeMillis());
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);

        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                long createid = db.insert(TableConsts.TBNAME_PLAYBACK, null, values);
                playlist.add(new MediaData(createid, id, MediaData.NOTPLAYED, data));
                if (isShuffle) {
                    addShuffle(shuffle_list, createid);
                }
                db.setTransactionSuccessful();
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }

    }

    public void addMedia(SQLiteDatabase db, String cur_contenturi, long id, String data) {
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_URI, cur_contenturi);
        values.put(TableConsts.PLAYBACK_MEDIA_ID, id);
        values.put(TableConsts.PLAYBACK_DATA, data);
        values.put(TableConsts.PLAYBACK_ORDER, System.currentTimeMillis());
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);

        long createid = db.insert(TableConsts.TBNAME_PLAYBACK, null, values);
        playlist.add(new MediaData(createid, id, MediaData.NOTPLAYED, data));
        if (isShuffle) {
            addShuffle(shuffle_list, createid);
        }
    }

    public ArrayList<MediaData> getList() {
        return playlist;
    }

    public ArrayList<Long> getNexList(int maxsize) {
        if (isShuffle) {
            return shuffle_list;
        }
        else {
            ArrayList<Long> nextlist = new ArrayList<Long>();
            int size = playlist.size();
            int pos = position + 1;
            for(int i=pos; i<size; i++){
                MediaData item = playlist.get(i);
                nextlist.add(item.id);
                if(maxsize>0 && nextlist.size()>maxsize){
                    break;
                }
            }
            
            return nextlist;
        }
    }

    public int getPosition() {
        return position;
    }

    public int getRestorePosition() {
        int ret = seekposition;
        seekposition = 0;
        Editor editor = pref.edit();
        editor.putInt(PREF_PLAYPOSITION, seekposition);
        editor.commit();

        return ret;
    }

    public void incRepeat() {
        if (repeatmode < 2) {
            repeatmode++;
        } else {
            repeatmode = 0;
        }

        Editor editor = pref.edit();
        editor.putInt(SystemConsts.PREF_REPEATFLG, getRepeatMode());
        editor.commit();
    }

    public void shuffle() {
        isShuffle = !isShuffle;
        if (isShuffle) {
            shuffle_list = makeShuffle();
        } else {
            shuffle_list.clear();
        }

        save(false);
    }

    public boolean clear() {
        int count = playlist.size();
        position = -1;
        seekposition = 0;

        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);

        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                db.delete(TableConsts.TBNAME_PLAYBACK, TableConsts.PLAYBACK_URI + " = ?",
                        new String[] {
                            cur_contenturi
                        });
                db.setTransactionSuccessful();
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }

        playlist = new ArrayList<MediaData>();
        shuffle_list = new ArrayList<Long>();

        save(false);

        return count > 0;
    }

    public boolean clearcut() {
        int befor_count = playlist.size();
        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                db.delete(TableConsts.TBNAME_PLAYBACK, TableConsts.PLAYBACK_URI + " = ? AND "
                        + TableConsts.PLAYBACK_STATE + " = ?",
                        new String[] {
                                cur_contenturi, Integer.toString(MediaData.NOTPLAYED)
                        });
                db.setTransactionSuccessful();
                loadPlaylist(db, cur_contenturi);
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }

        int count = playlist.size();
        return befor_count != count;
    }

    public MediaData getItem(int pos) {
        // 指定の位置のIDを取得するない場合はnull
        if (pos >= 0 && playlist.size() > pos) {
            return playlist.get(pos);
        }
        return null;
    }

    private long getNext(int pos) {
        if (isShuffle) {
            if (pos >= 0 && shuffle_list.size() > pos) {
                return shuffle_list.get(pos);
            }
        } else {
            int nextpos = position + pos+1;
            if(nextpos>=0 && playlist.size()>nextpos){
                return playlist.get(nextpos).id;
            }
        }
        return -1;
    }

    private void setPosition(MediaData id) {
        if (id != null) {
            if (isShuffle) {
                shuffle_list = makeShuffle();
            }
            setPositionID(id.id);
        } else {
            position = -1;
            seekposition = 0;
        }
    }

    public void setPosition(int pos) {
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();
                setPosition(db, pos);
                db.setTransactionSuccessful();
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }
    }

    private void setPosition(SQLiteDatabase db, int pos) {
        if (playlist.size() > 0 && pos < playlist.size()) {
            ArrayList<Long> tnextlist = new ArrayList<Long>();
            position = pos;
            seekposition = 0;
            for (int i = 0; i < playlist.size(); i++) {
                MediaData data = playlist.get(i);
                if (i <= pos) {
                    data.state = MediaData.PLAYED;
                    setPlayed(db, data.id);
                } else {
                    data.state = MediaData.NOTPLAYED;
                    tnextlist.add(data.id);
                }
            }
            if (isShuffle) {
                shuffle_list = makeShuffle();
            }
        } else {
            playlist.clear();
            shuffle_list.clear();
            position = -1;
            seekposition = 0;
        }
    }

    private void setPositionID(long id) {
        // 指定IDの位置にポジションを設定する、ない場合は−１
        if (id != -1) {
            for (int i = 0; i < playlist.size(); i++) {
                MediaData p = playlist.get(i);
                if (p.id == id) {
                    position = i;
                    seekposition = 0;
                    return;
                }
            }
        }
        position = -1;
        seekposition = 0;
    }

    public boolean removePosition(int pos) {
        int prepos = position;
        MediaData id = getItem(pos);
        MediaData cur = getItem(position);
        MediaData ncur = getItem(position + 1);
        if (id != null) {
            String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                    DeviceContentProvider.MEDIA_AUTHORITY);

            synchronized (dbLock) {
                SQLiteDatabase db = null;
                try {
                    db = getWritableDatabase();
                    db.beginTransaction();
                    int n = db.delete(TableConsts.TBNAME_PLAYBACK,
                            TableConsts.PLAYBACK_URI + " = ? AND " + BaseColumns._ID + " = ?",
                            new String[] {
                                    cur_contenturi, Long.toString(id.id)
                            });
                    db.setTransactionSuccessful();
                    if (n > 0) {

                        loadPlaylist(db, cur_contenturi);

                        if (cur == null || id.id != cur.id) {
                            setPosition(cur);
                        } else {
                            setPosition(ncur);
                        }

                        return pos == prepos;
                    }
                    else {
                        return false;
                    }
                } finally {
                    if (db != null && db.isOpen()) {
                        db.endTransaction();
                        //closeWritableDb();
                    }
                }
            }
        }
        return false;
    }

    private boolean setPlayed(SQLiteDatabase db, long id) {
        if (id != -1) {
            ContentValues values = new ContentValues();
            values.put(TableConsts.PLAYBACK_STATE, MediaData.PLAYED);
            db.update(TableConsts.TBNAME_PLAYBACK, values, BaseColumns._ID + " = ?", new String[] {
                    Long.toString(id)
            });

            return true;
        }
        return false;
    }

    public boolean setResetPlayed(SQLiteDatabase db, String cur_contenturi) {
        ContentValues values = new ContentValues();
        values.put(TableConsts.PLAYBACK_STATE, MediaData.NOTPLAYED);
        int n = db.update(TableConsts.TBNAME_PLAYBACK, values, TableConsts.PLAYBACK_URI + " = ? ",
                new String[] {
                    cur_contenturi
                });

        return n > 0;
    }

    public boolean moveNext() {
        if (repeatmode == SystemConsts.FLG_REPEAT_ONCE) {
            return position != -1;
        }

        String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        synchronized (dbLock) {
            SQLiteDatabase db = null;
            try {
                db = getWritableDatabase();
                db.beginTransaction();

                if(isShuffle){
                    if (shuffle_list.size() > 0) {
                        long next = shuffle_list.remove(0);
                        setPlayed(db, next);
                        setPositionID(next);
                        db.setTransactionSuccessful();
                        return true;
                    } else if (repeatmode == SystemConsts.FLG_REPEAT_ALL) {
                        //フラグをリセットして頭から
                        setResetPlayed(db, cur_contenturi);
                        shuffle_list = makeShuffle();
                        if (shuffle_list.size() > 0) {
                            long next = shuffle_list.remove(0);
                            setPlayed(db, next);
                            setPositionID(next);
                        }
                        else {
                            position = -1;
                            seekposition = 0;
                        }
                        db.setTransactionSuccessful();
                        return true;
                    } else {
                        //フラグをリセットして停止
                        setResetPlayed(db, cur_contenturi);
                        db.setTransactionSuccessful();
                        loadPlaylist(db, cur_contenturi);
                        position = -1;
                        seekposition = 0;
                    }
                }else{
                    int size = playlist.size();
                    if(size > 0){
                        int pos = position + 1;
                        if(size > pos){
                            MediaData item = playlist.get(pos);
                            setPlayed(db, item.id);
                            setPositionID(item.id);
                            db.setTransactionSuccessful();
                            return true;
                        }else if(repeatmode == SystemConsts.FLG_REPEAT_ALL){
                            //再読み込みして
                            setResetPlayed(db, cur_contenturi);
                            MediaData item = playlist.get(0);
                            setPlayed(db, item.id);
                            setPositionID(item.id);
                            db.setTransactionSuccessful();
                            return true;
                        } else {
                            //フラグをリセットして停止
                            setResetPlayed(db, cur_contenturi);
                            db.setTransactionSuccessful();
                            loadPlaylist(db, cur_contenturi);
                            position = -1;
                            seekposition = 0;
                        }
                    }
                }
            } finally {
                if (db != null && db.isOpen()) {
                    db.endTransaction();
                    //closeWritableDb();
                }
            }
        }

        return false;
    }

    public void moveBack(MediaData id) {
        if (id != null) {
            setPositionID(id.id);
        } else {
            position = -1;
            seekposition = 0;
        }
    }

    public MediaData getCurrent() {
        if (position < 0 && playlist.size() > 0) {
            position = 0;
            return getItem(0);
        } else {
            return getItem(position);
        }
    }

    public MediaData selectMediaData(long id) {
        for (MediaData data : playlist) {
            if (data.id == id) {
                return data;
            }
        }
        return null;
    }

    public MediaData getMediaData(int pos) {
        MediaData data = getItem(pos);
        if (data != null) {
            if (data.getDuration() <= 0 || data.getAlbum() == null) {
                Cursor media_cursor = null;
                try {
                    if (data.getDuration() <= 0 && data.mediaId > 0) {
                        media_cursor = mContext.getContentResolver().query(
                                ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI,
                                        data.mediaId), FETCH, null,
                                null, null);
                        if (media_cursor != null && media_cursor.moveToFirst()) {
                            String cur_contenturi = pref.getString(SystemConsts.PREF_CONTENTURI,
                                    DeviceContentProvider.MEDIA_AUTHORITY);
                            long duration = media_cursor.getLong(media_cursor
                                    .getColumnIndex(AudioMedia.DURATION));
                            data.setTitle(media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.TITLE)));
                            data.setAlbum(media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ALBUM)));
                            data.setArtist(media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ARTIST)));
                            String albumKey = media_cursor.getString(media_cursor
                                    .getColumnIndex(AudioMedia.ALBUM_KEY));
                            data.setDuration(duration);
                            if (duration > 0) {
                                ContentValues values = new ContentValues();
                                values.put(TableConsts.PLAYBACK_ALBUM_KEY, albumKey);
                                values.put(TableConsts.PLAYBACK_DURATION, duration);
                                synchronized (dbLock) {
                                    SQLiteDatabase db = null;
                                    try {
                                        db = getWritableDatabase();
                                        db.beginTransaction();
                                        db.update(TableConsts.TBNAME_PLAYBACK, values,
                                                TableConsts.PLAYBACK_URI + " = ? AND "
                                                        + BaseColumns._ID + " = ?", new String[] {
                                                        cur_contenturi, Long.toString(data.id)
                                                });
                                        db.setTransactionSuccessful();
                                    } finally {
                                        if (db != null && db.isOpen()) {
                                            db.endTransaction();
                                            //closeWritableDb();
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        synchronized (dbLock) {
                            SQLiteDatabase db = null;
                            Cursor cursor = null;
                            try {
                                db = getWritableDatabase();
                                cursor = db.query(TableConsts.TBNAME_PLAYBACK, null,
                                        BaseColumns._ID + " = ?", new String[] {
                                            Long.toString(data.id)
                                        }, null, null, null);
                                if (cursor != null && cursor.moveToFirst()) {
                                    data.setTitle(cursor.getString(cursor
                                            .getColumnIndex(TableConsts.PLAYBACK_TITLE)));
                                    data.setAlbum(cursor.getString(cursor
                                            .getColumnIndex(TableConsts.PLAYBACK_ALBUM)));
                                    data.setArtist(cursor.getString(cursor
                                            .getColumnIndex(TableConsts.PLAYBACK_ARTIST)));
                                    data.setDuration(cursor.getLong(cursor
                                            .getColumnIndex(TableConsts.PLAYBACK_DURATION)));
                                }
                            } finally {
                                if (cursor != null) {
                                    cursor.close();
                                }
                                if (db != null) {
                                    // db.close();
                                }
                            }
                        }
                    }
                    return data;
                } finally {
                    if (media_cursor != null) {
                        media_cursor.close();
                    }
                }
            }
            else {
                return data;
            }
        }
        return null;
    }

    public MediaData getNextPrefetch(int next) {
        long id = getNext(next);
        if (id != -1) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = getReadableDatabase();
                cursor = db.query(TableConsts.TBNAME_PLAYBACK, null, BaseColumns._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    MediaData data = new MediaData(
                            cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)),
                            cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID)),
                            cursor.getInt(cursor.getColumnIndex(TableConsts.PLAYBACK_STATE)),
                            cursor.getLong(cursor.getColumnIndex(TableConsts.PLAYBACK_DURATION)),
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_TITLE)),
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ALBUM)),
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_ARTIST)),
                            cursor.getString(cursor.getColumnIndex(TableConsts.PLAYBACK_DATA)));
                    return data;
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return null;
    }

    public int getBack(int pos, long duration) {
        // 全体の１０％以下なら頭出し、そうでなければ前の曲
        if (duration > 0 && pos > 0) {
            float pt = ((float) pos / duration) * 100.0f;
            if (pt > 10) {
                return position;
            } else {
                if (position > 0) {
                    return position - 1;
                } else {
                    return position;
                }
            }
        } else {
            if (position > 0) {
                return position - 1;
            } else {
                return position;
            }
        }
    }

    public boolean hasNext() {
        if (repeatmode == SystemConsts.FLG_REPEAT_ALL || repeatmode == SystemConsts.FLG_REPEAT_ONCE) {
            return playlist.size() > 0;
        }
        else {
            if (isShuffle) {
                return shuffle_list.size() > 0;
            } else {
                int size = playlist.size();
                return size > 0 && size > position;
            }
        }
    }
    
    public int findPosition(long id){
        for (int i=0; i<playlist.size(); i++) {
            MediaData item = playlist.get(i);
            if(id == item.id){
                return i;
            }
        }
        return -1;
    }

    public boolean drop(int from, int to) {
        return false;
    }

    private ArrayList<Long> makeShuffle() {
        int count = playlist.size();
        ArrayList<Long> shuffle = new ArrayList<Long>(count);
        for (MediaData item : playlist) {
            long id = item.id;
            int index = shuffle.size() > 0 ? rand.nextInt(shuffle.size()) : 0;
            if (shuffle.size() > index) {
                shuffle.add(index, id);
            } else {
                shuffle.add(id);
            }
        }
        return shuffle;
    }

    private void addShuffle(ArrayList<Long> list, long id) {
        int index = list.size() > 0 ? rand.nextInt(list.size()) : 0;
        if (list.size() > index) {
            list.add(index, id);
        } else {
            list.add(id);
        }
    }

    public GaplessMediaPlayer getMediaPlayer() {
        return mp;
    }

    public boolean isInitialize() {
        return mp != null;
    }

    public boolean Initialize(OnPreparedListener prepare,
            OnCompletionListener completion,
            OnBufferingUpdateListener bufering, OnErrorListener error) {
        // MediaPlayer
        mp = new GaplessMediaPlayer(mContext);
        mp.setOnPreparedListener(prepare);
        mp.setOnCompletionListener(completion);
        mp.setOnBufferingUpdateListener(bufering);
        mp.setLooping(false);
        mp.setOnErrorListener(error);

        // load playlist
        load();

        // autoPlay
        return true;
    }

    public SharedPreferences getPreferences() {
        return pref;
    }

    public boolean Release() {
        if (mDatabaseHelper != null) {
            closeWritableDb();
            closeReadableDb();
            mDatabaseHelper.close();
            mDatabaseHelper = null;
        }

        if (mp != null) {
            mp.release();
            mp = null;
            return true;
        }
        return false;
    }

    public boolean isPlaying() {
        if (mp != null) {
            return mp.isPlaying();
        }
        return false;
    }

    public boolean play() {
        if (mp != null && !mp.isPlaying()) {
            mp.start();
            return true;
        }
        return false;
    }

    public boolean pause() {
        if (mp != null && mp.isPlaying()) {
            mp.pause();
            return true;
        }
        return false;
    }

    public boolean stop() {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            return true;
        }
        return false;
    }

    public boolean reset() {
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
            return true;
        }
        return false;
    }

    @TargetApi(9)
    public int getAudioSessionId() {
        if (mp != null) {
            return mp.getAudioSessionId();
        }
        return -1;
    }

    public boolean prepareAsync() {
        if (mp != null) {
            mp.prepareAsync();
            return true;
        }
        return false;
    }

    public boolean setLooping(boolean b) {
        if (mp != null) {
            mp.setLooping(b);
            return true;
        }
        return false;
    }

    public boolean setDataSource(String path) throws IllegalArgumentException, SecurityException,
            IllegalStateException, IOException {
        if (mp != null) {
            mp.setDataSource(path);
            return true;
        }
        return false;
    }

    public boolean setDataSource(Context context, Uri uri) throws IllegalArgumentException,
            SecurityException, IllegalStateException, IOException {
        if (mp != null) {
            mp.setDataSource(context, uri);
            return true;
        }
        return false;
    }

    public int getCurrentPosition() {
        if (mp != null) {
            return mp.getCurrentPosition();
        }
        return 0;
    }

    public int getDuration() {
        if (mp != null) {
            return mp.getDuration();
        }
        return 0;
    }

    public boolean seekTo(int msec) {
        if (mp != null) {
            mp.seekTo(msec);
            return true;
        }
        return false;
    }

    public boolean setVolume(float vol) {
        if (mp != null) {
            mp.setVolume(vol, vol);
            return true;
        }

        return false;
    }
}
