
package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;

public class SystemInitService extends IntentService {
    private SharedPreferences mPref;

    public SystemInitService() {
        super("CacheCleanerService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPref = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Logger.d("Start cache clear");
        // Authクリア処理
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                getContentResolver().query(MediaConsts.PING_CONTENT_URI, null,
                        null, null, null);
            }
        });
        t.start();

        Logger.d("delete temp file");
        File[] tempfiles = SdCardAccessHelper.cachedMusicDir
                .listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String filename) {
                        // "temp", ".dat", rootdir
                        if (filename.startsWith("temp")) {
                            if (new File(dir, filename).length() < 2000000) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
        if (tempfiles != null) {
            for (File f : tempfiles) {
                f.delete();
            }
        }
    }

    private void showNotify(String msg) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Notification notification = new Notification(R.drawable.status, msg,
                System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, MainActivity2.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        notification.setLatestEventInfo(this, "JustPlayer initService",
                "cache manager", contentIntent);

        notificationManager.notify(R.string.hello, notification);

    }

    private void hideNotify() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(R.string.hello);
    }
}
