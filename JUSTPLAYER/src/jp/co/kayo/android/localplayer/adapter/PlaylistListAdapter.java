package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Calendar;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfo;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader;
import jp.co.kayo.android.localplayer.util.bean.PlaylistMemberInfo;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlaylistListAdapter extends ArrayAdapter<PlaylistInfo> {
    LayoutInflater inflator;
    Context context;
    ViewCache viewCache;
    Handler handler = new Handler();
    java.text.DateFormat format;

    public PlaylistListAdapter(Context context, List<PlaylistInfo> list, ViewCache cache) {
        super(context, R.layout.playlist_list_row, list);
        this.context = context;
        this.viewCache = cache;
        this.context = context;
        format = DateFormat.getLongDateFormat(context);
    }
    
    public LayoutInflater getInflator() {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = getInflator().inflate(R.layout.playlist_list_row, parent, false);
            holder = new ViewHolder();
            holder.setText1((TextView) convertView.findViewById(R.id.textTrack));
            holder.setText2((TextView) convertView.findViewById(R.id.textName));
            holder.setText3((TextView) convertView.findViewById(R.id.textModified));
            holder.setTextLabel((TextView) convertView.findViewById(R.id.textLabel));
            holder.setBackground(convertView.findViewById(R.id.background));
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        
        if (position % 2 == 1) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }
        
        PlaylistInfo item = getItem(position);
        if(item.id <= PlaylistInfoLoader.PLAYLIST_5START_ID){
            holder.getText1().setVisibility(View.GONE);
            holder.getText2().setVisibility(View.GONE);
            holder.getText3().setVisibility(View.GONE);
            holder.getTextLabel().setVisibility(View.VISIBLE);

            holder.getTextLabel().setText(item.name);
        }
        else{
            holder.getText1().setVisibility(View.VISIBLE);
            holder.getText2().setVisibility(View.VISIBLE);
            holder.getText3().setVisibility(View.VISIBLE);
            holder.getTextLabel().setVisibility(View.GONE);
            
            holder.getText1().setText(Funcs.getTrack(position + 1));
            holder.getText2().setText(item.name);
            holder.getText3().setText(format.format(item.modified));
        }

        return convertView;
    }
}
