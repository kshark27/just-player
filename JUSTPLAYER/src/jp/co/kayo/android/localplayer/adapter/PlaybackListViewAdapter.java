package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ImageObserverImpl;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import jp.co.kayo.android.localplayer.util.bean.PlaylistMemberInfo;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class PlaybackListViewAdapter extends IndexCursorAdapter {
    LayoutInflater inflator;
    Context context;
    SharedPreferences pref;
    private boolean isSdCard;
    private Map<Long, MediaData> mediaCache = new HashMap<Long, MediaData>();
    private final String[] FETCH = new String[]{ AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ARTIST, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY, AudioMedia.DATA, AudioMedia.DURATION};

    int getColId(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_MEDIA_ID);
    }

    int getColTitle(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_TITLE);
    }

    int getColArtist(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_ARTIST);
    }

    int getColAlbum(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_ALBUM);
    }

    int getColDuration(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_DURATION);
    }

    int getColData(Cursor cursor) {
        return cursor.getColumnIndex(TableConsts.PLAYBACK_DATA);
    }
    
    int getPlaybackState(Cursor cursor){
        return cursor.getColumnIndex(TableConsts.PLAYBACK_STATE);
    }

    int getColRating(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioMedia.FAVORITE_POINT);
    }

    public PlaybackListViewAdapter(Context context, Cursor c, ViewCache cache) {
        super(context, c, true, cache, TableConsts.PLAYBACK_TITLE);
        this.context = context;
        this.pref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }
    
    private MediaData loadMediaItem(long id) {
        Cursor cursor = null;
        try{
            cursor = context.getContentResolver().query(
                    ContentUris.withAppendedId(MediaConsts.MEDIA_CONTENT_URI, id), FETCH, null,
                    null, null);

            if(cursor != null && cursor.moveToFirst()){
                MediaData data = new MediaData(
                        0,
                        cursor.getLong(cursor.getColumnIndex(AudioMedia._ID)), 
                        MediaData.NOTPLAYED, 
                        cursor.getLong(cursor.getColumnIndex(AudioMedia.DURATION)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.TITLE)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ALBUM)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.ARTIST)), 
                        cursor.getString(cursor.getColumnIndex(AudioMedia.DATA)));
                return data;
            }
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
        
        return null;
    }

    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (position % 2 == 1) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(getColId(cursor));
        String title = cursor.getString(getColTitle(cursor));
        String album = cursor.getString(getColAlbum(cursor));
        String artist = cursor.getString(getColArtist(cursor));
        long duration = cursor.getLong(getColDuration(cursor));
        String data = cursor.getString(getColData(cursor));
        MediaData item = mediaCache.get(id);
        if((item == null || item.getDuration()<=0) && id>0){
            if(!imageloadskip){
                item = loadMediaItem(id);
                if(item!=null){
                    mediaCache.put(id, item);
                }
                else{
                    //多分削除されたItem
                }
            }
        }
        if(item != null){
            title = item.getTitle();
            album = item.getAlbum();
            artist = item.getArtist();
            duration = item.getDuration();
        }
        if(!imageloadskip){
            FavoriteInfo inf = getViewcache().getFavorite(context, id,
                    TableConsts.FAVORITE_TYPE_SONG);
            holder.getRating1().setRating(inf.rating);
        }
        else{
            holder.getRating1().setRating(0);
        }

        int pos = cursor.getPosition();
        holder.getText1().setText(Funcs.getTrack(pos + 1));
        holder.getText2().setText(title);
        holder.getText3().setText(album);
        holder.getText4().setText(artist);        
        holder.getText5().setText(Funcs.makeTimeString(duration, false));

        
        
        if (isSdCard != true && SdCardAccessHelper.existCachFile(data)) {
            holder.getImgCache().setVisibility(View.VISIBLE);
        } else {
            holder.getImgCache().setVisibility(View.GONE);
        }

        if (getViewcache().getCurrentId() == id) {
            holder.getImgPlay().setVisibility(View.VISIBLE);
        } else {
            holder.getImgPlay().setVisibility(View.GONE);
        }
        if (getViewcache().getPrefetchId() == -1
                || getViewcache().getPrefetchId() != id) {
            holder.getProgressBar1().setVisibility(View.GONE);
        } else {
            if (getViewcache().getMax() > 0) {
                holder.getProgressBar1().setVisibility(View.VISIBLE);
                holder.getProgressBar1().setMax(
                        (int) (getViewcache().getMax() / 1000));
                holder.getProgressBar1().setProgress(
                        (int) (getViewcache().getPos() / 1000));
            } else {
                holder.getProgressBar1().setVisibility(View.GONE);
            }
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.text_list_row3, parent,
                false);
        ViewHolder holder = new ViewHolder();
        holder.setText1((TextView) v.findViewById(R.id.text1));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText1());
        holder.setText2((TextView) v.findViewById(R.id.text2));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setText3((TextView) v.findViewById(R.id.text3));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
        holder.setText4((TextView) v.findViewById(R.id.text4));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText4());
        holder.setText5((TextView) v.findViewById(R.id.text5));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText5());
        holder.setRating1((RatingBar) v.findViewById(R.id.ratingBar1));
        holder.setImgCache((ImageView) v.findViewById(R.id.imgCache));
        holder.setImgPlay(v.findViewById(R.id.imgPlay));
        holder.setProgressBar1((ProgressBar) v.findViewById(R.id.progressBar1));
        holder.setBackground(v.findViewById(R.id.background));
        v.setTag(holder);
        return v;
    }
}
