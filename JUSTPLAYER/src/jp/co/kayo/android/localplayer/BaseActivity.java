package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.util.Hashtable;


import jp.co.kayo.android.exceptionlib.ExceptionBinder;
import jp.co.kayo.android.localplayer.MyTabsAdapter.TabInfo;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.secret.Keys;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.util.NfcHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.app.ActionBar;
import android.app.ActionBar.Tab;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuInflater;

public abstract class BaseActivity extends FragmentActivity implements
        ServiceBinderHolder {
    IMediaPlayerService mBinder;
    private NfcHelper mNfchelper = null;

    public IMediaPlayerService getBinder() {
        return mBinder;
    }

    abstract IMediaPlayerServiceCallback getCallBack();

    abstract ViewCache getViewCache();

    abstract Handler getHandler();

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBinder = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = IMediaPlayerService.Stub.asInterface(service);
            try {
                // サービスからの通知をうけとれるようにコールバックを登録
                if (getCallBack() != null) {
                    mBinder.registerCallback(getCallBack());
                }
                if (getViewCache() != null) {
                    try {
                        long media_id = mBinder.getMediaId();
                        getViewCache().setPosition(mBinder.getPosition(),
                                media_id, mBinder.getPrefetchId());
                    } catch (RemoteException e) {
                    }
                }
                FragmentManager m = getSupportFragmentManager();
                ControlFragment control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
                if (control != null) {
                    control.updateView();
                }
                
            } catch (RemoteException e) {
            }

            BaseActivity.this.onServiceConnected(mBinder);
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        ExceptionBinder.bind(this, Keys.EXCEPTION_KEY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= 14) {
            if (mNfchelper == null && getHandler() != null) {
                mNfchelper = new NfcHelper(this, getHandler());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setActionBarSubTitle(int stat) {
        if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
            IMediaPlayerService binder = getBinder();
            long media_id;
            try {
                media_id = binder.getMediaId();
                if (media_id > 0) {
                    Hashtable<String, String> tbl1 = ContentsUtils.getMedia(
                            this, new String[] { AudioMedia.ALBUM,
                                    AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
                                    AudioMedia.TITLE }, media_id);
                    String title = tbl1.get(AudioMedia.TITLE);
                    String artist = tbl1.get(AudioMedia.ARTIST);
                    getSupportActionBar().setSubtitle(title + " - " + artist);
                } else {
                    int pos = binder.getPosition();
                    String[] values = binder.getMediaD(pos);
                    if (values != null) {
                        String title = values[0];
                        String artist = values[2];
                        getSupportActionBar().setSubtitle(
                                title + " - " + artist);
                    }
                }
            } catch (RemoteException e) {
            }
        } else {
            getSupportActionBar().setSubtitle(getString(R.string.hello));
        }
    }

    public void onServiceConnected(IMediaPlayerService binder) {
    }
    
    public Fragment getCurrentFragment() {
        Tab tab = getSupportActionBar().getSelectedTab();
        Object tag = tab.getTag();
        if(tag instanceof TabInfo){
            TabInfo info = (TabInfo) tab.getTag();
            if (info != null) {
                return getSupportFragmentManager().findFragmentByTag(
                        info.fragment_tag);
            }
        }else{
            return getSupportFragmentManager().findFragmentById(R.id.container);
        }
        return null;
    }
    
    public void selectTab(int index){
        if (getSupportActionBar().getTabCount() > index) {
            getSupportActionBar().setSelectedNavigationItem(index);
        }
        for (int i = 0; i < getSupportActionBar().getTabCount(); i++) {
            Tab tab = getSupportActionBar().getTabAt(i);
            Object object = tab.getTag();
            Fragment f;
            if(object instanceof TabInfo){
                TabInfo info = (TabInfo) object;
                f = getSupportFragmentManager().findFragmentByTag(info.fragment_tag);
            }
            else{
                f = getSupportFragmentManager().findFragmentById(R.id.container);
            }
            if (f != null && f instanceof ContextMenuFragment) {
                ((ContextMenuFragment)f).hideMenu();
            }
        }
    }    

    public NfcHelper getNFCHelper() {
        return mNfchelper;
    }

    public void showProgressBar() {
        setProgressBarIndeterminateVisibility(true);
        //setSupportProgressBarIndeterminateVisibility(true);
    }

    public void hideProgressBar() {
        setProgressBarIndeterminateVisibility(false);
        //setSupportProgressBarIndeterminateVisibility(false);
    }

    public ActionBar getSupportActionBar() {
        return getActionBar();
    }

    public MenuInflater getSupportMenuInflater() {
        return getMenuInflater();
    }
}
