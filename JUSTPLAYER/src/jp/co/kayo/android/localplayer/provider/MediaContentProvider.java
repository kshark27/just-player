package jp.co.kayo.android.localplayer.provider;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.FileNotFoundException;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.db.JustPlayerDatabaseHelper;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.UriMatcher;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;

/***
 * このクラスは複数のコンテンツプロバイダの橋渡しをするクラスです
 * 
 * @author yokmama
 */
public class MediaContentProvider extends ContentProvider implements
        MediaConsts {
    private SharedPreferences pref;
    private JustPlayerDatabaseHelper helper;
    private UriMatcher uriMatcher = null;

    @Override
    public boolean onCreate() {
        pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        helper = new JustPlayerDatabaseHelper(getContext());
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(MediaConsts.AUTHORITY, "pref", CODE_PREF);
        
        return false;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    private String getContentUri() {
        String ret = pref.getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
        return ret;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                getContentUri());
        return getContext().getContentResolver().delete(Uri.parse(u),
                selection, selectionArgs);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                getContentUri());
        return getContext().getContentResolver().insert(Uri.parse(u), values);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                getContentUri());
        return getContext().getContentResolver().query(Uri.parse(u),
                projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        if(match == CODE_PREF){
            if(values.containsKey(SystemConsts.PREF_CONTENTURI)){
                Editor editor = pref.edit();
                editor.putString(SystemConsts.PREF_CONTENTURI, values.getAsString(SystemConsts.PREF_CONTENTURI));
                editor.commit();
                getContext().getContentResolver().notifyChange(
                        ARTIST_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ALBUM_CONTENT_URI, null);
            }
            return 0;
        }
        else{
            String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                    getContentUri());
            return getContext().getContentResolver().update(Uri.parse(u), values,
                    selection, selectionArgs);
        }
    }
    
    @Override
    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                getContentUri());
        
        return getContext().getContentResolver().openAssetFileDescriptor(Uri.parse(u), mode);
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        String u = uri.toString().replaceFirst(MediaConsts.AUTHORITY,
                getContentUri());
        return getContext().getContentResolver().openFileDescriptor(Uri.parse(u), mode);
    }

    private int downloadDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_DOWNLOAD, selection,
                    selectionArgs);
            db.endTransaction();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    private int downloadIdDelete(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            long id = ContentUris.parseId(uri);
            db.beginTransaction();
            int ret = db.delete(TableConsts.TBNAME_DOWNLOAD, BaseColumns._ID
                    + " = ?", new String[] { Long.toString(id) });
            db.endTransaction();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    private Cursor downloadQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            db = helper.getReadableDatabase();
            return db.query(TableConsts.TBNAME_DOWNLOAD, projection, selection,
                    selectionArgs, null, null, sortOrder);
        } finally {
        }
    }

    private Cursor downloadIdQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = helper.getReadableDatabase();
            return db.query(TableConsts.TBNAME_DOWNLOAD, projection,
                    BaseColumns._ID + " = ?",
                    new String[] { Long.toString(id) }, null, null, sortOrder);
        } finally {
        }
    }

    /*
     * private Uri downloadInsert(Uri uri, ContentValues values) {
     * SQLiteDatabase db = null; try { db = helper.getWritableDatabase();
     * db.beginTransaction(); long id = db.insert(TableConsts.TBNAME_DOWNLOAD,
     * null, values); db.setTransactionSuccessful(); return
     * ContentUris.withAppendedId(MediaConsts.DOWNLOAD_CONTENT_URI, id); }
     * finally { db.endTransaction(); } }
     */

    private int downloadUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_DOWNLOAD, values, selection,
                    selectionArgs);
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }

    private int downloadIdUpdate(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = helper.getWritableDatabase();
            db.beginTransaction();
            int ret = db.update(TableConsts.TBNAME_DOWNLOAD, values,
                    BaseColumns._ID + " = ?",
                    new String[] { Long.toString(id) });
            db.setTransactionSuccessful();
            return ret;
        } finally {
            db.endTransaction();
        }
    }
}
