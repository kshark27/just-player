package jp.co.kayo.android.localplayer.appwidget;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.InputStream;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RemoteViews;

public class AppWidget1 extends AppWidgetProvider {
    private static String TAG = "AppWidget1";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
            int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        Intent i = new Intent(context, MediaPlayerService.class);
        i.setAction(AppWidgetHelper.CALL_UPDATEWIDGET);
        context.startService(i);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (hasInstances(context)) {
            if (AppWidgetHelper.CALL_APPWIDGET_DISPLAY.equals(intent
                    .getAction())) {
                Intent i = new Intent(context, MyService.class);
                i.setAction("APPWIDGET_DISPLAY");
                i.putExtra("mediaId", intent.getStringExtra("mediaId"));
                i.putExtra("artist", intent.getStringExtra("artist"));
                i.putExtra("title", intent.getStringExtra("title"));
                i.putExtra("album", intent.getStringExtra("album"));
                i.putExtra("stat", intent.getIntExtra("stat", 0));
                context.startService(i);
            }
        }
    }

    private boolean hasInstances(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        int[] appWidgetIds = appWidgetManager
                .getAppWidgetIds(new ComponentName(context, this.getClass()));
        return (appWidgetIds.length > 0);
    }

    public static class MyService extends Service {
        SharedPreferences mPref;

        @Override
        public void onCreate() {
            super.onCreate();
            mPref = PreferenceManager.getDefaultSharedPreferences(this);
        }

        private PendingIntent createIntent(Context context, String action) {
            Intent i = new Intent(context, MediaPlayerService.class);
            i.setAction(action);
            return PendingIntent.getService(this, 0, i, 0);
        }

        @Override
        public void onStart(Intent intent, int _startId) {
            if (intent != null
                    && "APPWIDGET_DISPLAY".equals(intent.getAction())) {
                String mediaId = intent.getStringExtra("mediaId");
                String artist = intent.getStringExtra("artist");
                String title = intent.getStringExtra("title");
                String album = intent.getStringExtra("album");
                int stat = intent.getIntExtra("stat", 0);
                updateWidgetTitle(mediaId, artist, title, album, stat);
            }

            stopSelf();
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        public Bitmap loadBitmap(Context context, Uri uri, int fitsize) {
            InputStream is = null;
            Bitmap bmp = null;
            try {
                // 読み込む画像へのUriがある場合は読み込む
                if (uri != null) {
                    // まずはオリジナルの縦と横のサイズを取得するため、
                    // 画像読み込みなし(inJustDecodeBounds = true)で画像を読み込む
                    is = context.getContentResolver().openInputStream(uri);
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(is, null, opts);
                    is.close();
                    is = null;

                    // fitsizeに合わせてX方向とY方向それぞれのinSampleSizeを求める
                    int sample_x = 1 + (opts.outWidth / fitsize);
                    int sample_y = 1 + (opts.outHeight / fitsize);

                    is = context.getContentResolver().openInputStream(uri);
                    opts.inSampleSize = Math.max(sample_x, sample_y);
                    opts.inJustDecodeBounds = false;
                    bmp = BitmapFactory.decodeStream(is, null, opts);
                    return bmp;
                }
            } catch (Exception e) {
                Logger.e("loadBitmap", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e) {
                    }
                }
            }
            return null;
        }

        public Bitmap updateShape(Context context, RemoteViews remoteViews) {
            //frame
            boolean isFrame = mPref.getBoolean(DisplayPreference.KEY_DISPFRAME, true);
            //solid shape
            AppWidgetManager manager = AppWidgetManager.getInstance(context);
            if (manager != null) {
                int fontcolor = mPref.getInt(DisplayPreference.KEY_FONTCOLOR,
                        DisplayPreference.default_fontcolor);
                int backcolor = mPref.getInt(DisplayPreference.KEY_BACKCOLOR,
                        DisplayPreference.default_backcolor);

                float[] outerR = isFrame?new float[] { 5, 5, 5, 5, 5, 5, 5, 5 }: new float[] { 0, 0, 0, 0, 0, 0, 0, 0 };
                ShapeDrawable shape = new ShapeDrawable(new RoundRectShape(
                        outerR, null, null));
                shape.getPaint().setStyle(Style.FILL_AND_STROKE);
                shape.getPaint().setColor(backcolor);
                Drawable d = shape.getCurrent();
                DisplayMetrics metrics = new DisplayMetrics();
                Display display = ((WindowManager) context
                        .getSystemService(Context.WINDOW_SERVICE))
                        .getDefaultDisplay();
                display.getMetrics(metrics);

                int wdip = (2 * 72)-2;
                int hdip = (1 * 72) -2;

                int w = (int) (wdip * metrics.scaledDensity);
                int h = (int) (hdip * metrics.scaledDensity);

                Bitmap bitmap = Bitmap.createBitmap(w, h,
                        Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                d.setBounds(0, 0, w, h);
                d.draw(canvas);

                remoteViews.setImageViewBitmap(R.id.backPanel2, bitmap);
                if(Build.VERSION.SDK_INT>7){
                    if(!isFrame){
                        remoteViews.setInt(R.id.backPanel1, "setBackgroundResource", R.drawable.trans);
                    }else{
                        remoteViews.setInt(R.id.backPanel1, "setBackgroundResource", R.drawable.widget_background);
                    }
                }
                remoteViews.setTextColor(R.id.textTitle, fontcolor);
                remoteViews.setTextColor(R.id.textArtist, fontcolor);
                return bitmap;
            } else {
                return null;
            }
        }

        private void updateWidgetTitle(String mediaId, String artist, String title,
                String album, int stat) {
            Log.d(TAG, "AppWidget1.MyService.updateWidgetTitle");

            Bitmap bitmap = null;
            try {
                RemoteViews remoteView = new RemoteViews(getPackageName(),
                        R.layout.widget1);
                
                Hashtable<String, String> tbl1 = new Hashtable<String, String>(), tbl2 = new Hashtable<String, String>();
                Bitmap bmp = null;
                if(mediaId!=null && mediaId.length()>0){
                    bmp = Funcs.getAlbumArt(this, Long.parseLong(mediaId), R.drawable.albumart_mp_unknown, tbl1, tbl2);
                    if (bmp != null) {
                        remoteView.setImageViewBitmap(R.id.imageView1, bmp);
                    } else {
                        remoteView.setImageViewResource(R.id.imageView1,
                                R.drawable.albumart_mp_unknown);
                    }
                }
                else{
                    Integer key = AppWidgetHelper.getAlbumKey(album, artist);
                    File file = AppWidgetHelper.createAlbumArtFile(key);
                    if (file != null && file.exists()) {
                        bmp = loadBitmap(this, Uri.fromFile(file), 128);
                        if (bmp != null) {
                            remoteView.setImageViewBitmap(R.id.imageView1, bmp);
                        } else {
                            remoteView.setImageViewResource(R.id.imageView1,
                                    R.drawable.albumart_mp_unknown);
                        }
                    } else {
                        remoteView.setImageViewResource(R.id.imageView1,
                                R.drawable.albumart_mp_unknown);
                    }
                }

                remoteView.setOnClickPendingIntent(R.id.btnPlay,
                        createIntent(this, AppWidgetHelper.CALL_PLAY_PAUSE));
                remoteView.setOnClickPendingIntent(R.id.btnFf,
                        createIntent(this, AppWidgetHelper.CALL_FF));
                remoteView.setOnClickPendingIntent(R.id.btnRew,
                        createIntent(this, AppWidgetHelper.CALL_REW));
                remoteView.setOnClickPendingIntent(R.id.btnRept1,
                        createIntent(this, AppWidgetHelper.CALL_LOOP));
                
                if(tbl1.size()>0){
                    remoteView.setTextViewText(R.id.textTitle,
                            Funcs.trimString(tbl1.get(AudioMedia.TITLE)));
                    remoteView.setTextViewText(R.id.textArtist,
                            Funcs.trimString(tbl1.get(AudioMedia.ARTIST)));
                }
                else{
                    remoteView.setTextViewText(R.id.textTitle,
                            Funcs.trimString(title));
                    remoteView.setTextViewText(R.id.textArtist,
                            Funcs.trimString(artist));
                }

                Intent intent = new Intent(this, MainActivity2.class);
                intent.setAction(SystemConsts.MAIN_ACITON_SHOWHOMW);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0, intent, 0);
                //remoteView.setOnClickPendingIntent(R.id.layout1, pendingIntent);
                remoteView.setOnClickPendingIntent(R.id.imageView1,pendingIntent);

                bitmap = updateShape(this, remoteView);

                // play
                if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
                    remoteView.setImageViewResource(R.id.btnPlay,
                            R.drawable.widget_pause);
                } else {
                    remoteView.setImageViewResource(R.id.btnPlay,
                            R.drawable.widget_play);
                }
                // rew
                remoteView.setImageViewResource(R.id.btnRew,
                        R.drawable.widget_previous);
                // ff
                remoteView.setImageViewResource(R.id.btnFf,
                        R.drawable.widget_next);

                AppWidgetManager manager = AppWidgetManager.getInstance(this);
                ComponentName widget = new ComponentName(this, AppWidget1.class);
                manager.updateAppWidget(widget, remoteView);
            } finally {
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        }
    }
}
