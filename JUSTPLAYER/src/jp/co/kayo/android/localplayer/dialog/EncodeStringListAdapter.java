package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.dialog.EnocdeStringListDialog.EncodeStringItems;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EncodeStringListAdapter extends ArrayAdapter<EncodeStringItems> {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();
    private String mEncodingLabel;

    public EncodeStringListAdapter(Context context,
            ArrayList<EncodeStringItems> items) {
        super(context, -1, items);
        this.context = context;
        mEncodingLabel = context.getString(R.string.lb_encoding);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = getInflator(context).inflate(
                    R.layout.encode_string_row, parent, false);
            holder = new ViewHolder();
            holder.str = (TextView) convertView.findViewById(R.id.encodedstr);
            holder.enc = (TextView) convertView.findViewById(R.id.encoding);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        EncodeStringItems item = getItem(position);

        holder.str.setText(item.str);
        holder.enc.setText(mEncodingLabel + item.enc);

        return convertView;
    }

    private class ViewHolder {
        TextView str;
        TextView enc;
    }
}
